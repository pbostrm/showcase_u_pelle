System:
  OS: Windows 7 Service Pack 1 (6.1.7601) 64bit
  CPU: Intel(R) Xeon(R) CPU X5450 @ 3.00GHz, count: 8
  Physical RAM: 8190 MB
  Addressable RAM: 2048 MB

D3D9:
  Renderer: NVIDIA GeForce GTX 560 Ti  
  VendorID: 10de
  Driver: nvd3dum.dll 9.18.13.1106
  VRAM: 978 (via DXGI)

OpenGL:
  Version: 4.3.0
  Vendor: NVIDIA Corporation
  Renderer: GeForce GTX 560 Ti/PCIe/SSE2
  VRAM: 978 (via DXGI)
  Extensions: GL_AMD_multi_draw_indirect GL_ARB_arrays_of_arrays GL_ARB_base_instance GL_ARB_blend_func_extended GL_ARB_clear_buffer_object GL_ARB_color_buffer_float GL_ARB_compatibility GL_ARB_compressed_texture_pixel_storage GL_ARB_conservative_depth GL_ARB_compute_shader GL_ARB_copy_buffer GL_ARB_copy_image GL_ARB_debug_output GL_ARB_depth_buffer_float GL_ARB_depth_clamp GL_ARB_depth_texture GL_ARB_draw_buffers GL_ARB_draw_buffers_blend GL_ARB_draw_indirect GL_ARB_draw_elements_base_vertex GL_ARB_draw_instanced GL_ARB_ES2_compatibility GL_ARB_ES3_compatibility GL_ARB_explicit_attrib_location GL_ARB_explicit_uniform_location GL_ARB_fragment_coord_conventions GL_ARB_fragment_layer_viewport GL_ARB_fragment_program GL_ARB_fragment_program_shadow GL_ARB_fragment_shader GL_ARB_framebuffer_no_attachments GL_ARB_framebuffer_object GL_ARB_framebuffer_sRGB GL_ARB_geometry_shader4 GL_ARB_get_program_binary GL_ARB_gpu_shader5 GL_ARB_gpu_shader_fp64 GL_ARB_half_float_pixel GL_ARB_half_float_vertex GL_ARB_imaging GL_ARB_instanced_arrays GL_ARB_internalformat_query GL_ARB_internalformat_query2 GL_ARB_invalidate_subdata GL_ARB_map_buffer_alignment GL_ARB_map_buffer_range GL_ARB_multi_draw_indirect GL_ARB_multisample GL_ARB_multitexture GL_ARB_occlusion_query GL_ARB_occlusion_query2 GL_ARB_pixel_buffer_object GL_ARB_point_parameters GL_ARB_point_sprite GL_ARB_program_interface_query GL_ARB_provoking_vertex GL_ARB_robust_buffer_access_behavior GL_ARB_robustness GL_ARB_sample_shading GL_ARB_sampler_objects GL_ARB_seamless_cube_map GL_ARB_separate_shader_objects GL_ARB_shader_atomic_counters GL_ARB_shader_bit_encoding GL_ARB_shader_image_load_store GL_ARB_shader_image_size GL_ARB_shader_objects GL_ARB_shader_precision GL_ARB_shader_storage_buffer_object GL_ARB_shader_subroutine GL_ARB_shader_texture_lod GL_ARB_shading_language_100 GL_ARB_shading_language_420pack GL_ARB_shading_language_include GL_ARB_shading_language_packing GL_ARB_shadow GL_ARB_stencil_texturing GL_ARB_sync GL_ARB_tessellation_shader GL_ARB_texture_border_clamp GL_ARB_texture_buffer_object GL_ARB_texture_buffer_object_rgb32 GL_ARB_texture_buffer_range GL_ARB_texture_compression GL_ARB_texture_compression_bptc GL_ARB_texture_compression_rgtc GL_ARB_texture_cube_map GL_ARB_texture_cube_map_array GL_ARB_texture_env_add GL_ARB_texture_env_combine GL_ARB_texture_env_crossbar GL_ARB_texture_env_dot3 GL_ARB_texture_float GL_ARB_texture_gather GL_ARB_texture_mirrored_repeat GL_ARB_texture_multisample GL_ARB_texture_non_power_of_two GL_ARB_texture_query_levels GL_ARB_texture_query_lod GL_ARB_texture_rectangle GL_ARB_texture_rg GL_ARB_texture_rgb10_a2ui GL_ARB_texture_storage GL_ARB_texture_storage_multisample GL_ARB_texture_swizzle GL_ARB_texture_view GL_ARB_timer_query GL_ARB_transform_feedback2 GL_ARB_transform_feedback3 GL_ARB_transform_feedback_instanced GL_ARB_transpose_matrix GL_ARB_uniform_buffer_object GL_ARB_vertex_array_bgra GL_ARB_vertex_array_object GL_ARB_vertex_attrib_64bit GL_ARB_vertex_attrib_binding GL_ARB_vertex_buffer_object GL_ARB_vertex_program GL_ARB_vertex_shader GL_ARB_vertex_type_2_10_10_10_rev GL_ARB_viewport_array GL_ARB_window_pos GL_ATI_draw_buffers GL_ATI_texture_float GL_ATI_texture_mirror_once GL_S3_s3tc GL_EXT_texture_env_add GL_EXT_abgr GL_EXT_bgra GL_EXT_bindable_uniform GL_EXT_blend_color GL_EXT_blend_equation_separate GL_EXT_blend_func_separate GL_EXT_blend_minmax GL_EXT_blend_subtract GL_EXT_compiled_vertex_array GL_EXT_Cg_shader GL_EXT_depth_bounds_test GL_EXT_direct_state_access GL_EXT_draw_buffers2 GL_EXT_draw_instanced GL_EXT_draw_range_elements GL_EXT_fog_coord GL_EXT_framebuffer_blit GL_EXT_framebuffer_multisample GL_EXTX_framebuffer_mixed_formats GL_EXT_framebuffer_multisample_blit_scaled GL_EXT_framebuffer_object GL_EXT_framebuffer_sRGB GL_EXT_geometry_shader4 GL_EXT_gpu_program_parameters GL_EXT_gpu_shader4 GL_EXT_multi_draw_arrays GL_EXT_packed_depth_stencil GL_EXT_packed_float GL_EXT_packed_pixels GL_EXT_pixel_buffer_object GL_EXT_point_parameters GL_EXT_provoking_vertex GL_EXT_rescale_normal GL_EXT_secondary_color GL_EXT_separate_shader_objects GL_EXT_separate_specular_color GL_EXT_shader_image_load_store GL_EXT_shadow_funcs GL_EXT_stencil_two_side GL_EXT_stencil_wrap GL_EXT_texture3D GL_EXT_texture_array GL_EXT_texture_buffer_object GL_EXT_texture_compression_dxt1 GL_EXT_texture_compression_latc GL_EXT_texture_compression_rgtc GL_EXT_texture_compression_s3tc GL_EXT_texture_cube_map GL_EXT_texture_edge_clamp GL_EXT_texture_env_combine GL_EXT_texture_env_dot3 GL_EXT_texture_filter_anisotropic GL_EXT_texture_format_BGRA8888 GL_EXT_texture_integer GL_EXT_texture_lod GL_EXT_texture_lod_bias GL_EXT_texture_mirror_clamp GL_EXT_texture_object GL_EXT_texture_shared_exponent GL_EXT_texture_sRGB GL_EXT_texture_sRGB_decode GL_EXT_texture_storage GL_EXT_texture_swizzle GL_EXT_texture_type_2_10_10_10_REV GL_EXT_timer_query GL_EXT_transform_feedback2 GL_EXT_vertex_array GL_EXT_vertex_array_bgra GL_EXT_vertex_attrib_64bit GL_EXT_import_sync_object GL_IBM_rasterpos_clip GL_IBM_texture_mirrored_repeat GL_KHR_debug GL_KTX_buffer_region GL_NV_alpha_test GL_NV_blend_minmax GL_NV_blend_square GL_NV_complex_primitives GL_NV_compute_program5 GL_NV_conditional_render GL_NV_copy_depth_to_color GL_NV_copy_image GL_NV_depth_buffer_float GL_NV_depth_clamp GL_NV_draw_texture GL_NV_ES1_1_compatibility GL_NV_explicit_multisample GL_NV_fbo_color_attachments GL_NV_fence GL_NV_float_buffer GL_NV_fog_distance GL_NV_fragdepth GL_NV_fragment_program GL_NV_fragment_program_option GL_NV_fragment_program2 GL_NV_framebuffer_multisample_coverage GL_NV_geometry_shader4 GL_NV_gpu_program4 GL_NV_gpu_program4_1 GL_NV_gpu_program5 GL_NV_gpu_program_fp64 GL_NV_gpu_shader5 GL_NV_half_float GL_NV_light_max_exponent GL_NV_multisample_coverage GL_NV_multisample_filter_hint GL_NV_occlusion_query GL_NV_packed_depth_stencil GL_NV_parameter_buffer_object GL_NV_parameter_buffer_object2 GL_NV_path_rendering GL_NV_pixel_data_range GL_NV_point_sprite GL_NV_primitive_restart GL_NV_register_combiners GL_NV_register_combiners2 GL_NV_shader_atomic_counters GL_NV_shader_atomic_float GL_NV_shader_buffer_load GL_NV_shader_storage_buffer_object GL_NV_texgen_reflection GL_NV_texture_barrier GL_NV_texture_compression_vtc GL_NV_texture_env_combine4 GL_NV_texture_expand_normal GL_NV_texture_lod_clamp GL_NV_texture_multisample GL_NV_texture_rectangle GL_NV_texture_shader GL_NV_texture_shader2 GL_NV_texture_shader3 GL_NV_transform_feedback GL_NV_transform_feedback2 GL_NV_vertex_array_range GL_NV_vertex_array_range2 GL_NV_vertex_attrib_integer_64bit GL_NV_vertex_buffer_unified_memory GL_NV_vertex_program GL_NV_vertex_program1_1 GL_NV_vertex_program2 GL_NV_vertex_program2_option GL_NV_vertex_program3 GL_NVX_conditional_render GL_NVX_gpu_memory_info GL_OES_compressed_paletted_texture GL_OES_depth24 GL_OES_depth32 GL_OES_depth_texture GL_OES_element_index_uint GL_OES_fbo_render_mipmap GL_OES_get_program_binary GL_OES_mapbuffer GL_OES_packed_depth_stencil GL_OES_point_size_array GL_OES_point_sprite GL_OES_rgb8_rgba8 GL_OES_read_format GL_OES_standard_derivatives GL_OES_texture_3D GL_OES_texture_float GL_OES_texture_float_linear GL_OES_texture_half_float GL_OES_texture_half_float_linear GL_OES_texture_npot GL_OES_vertex_array_object GL_OES_vertex_half_float GL_SGIS_generate_mipmap GL_SGIS_texture_lod GL_SGIX_depth_texture GL_SGIX_shadow GL_SUN_slice_accum GL_WIN_swap_hint WGL_EXT_swap_control 

User locale: sv-SE
System locale: sv-SE

Running processes:
Skype.exe (2820)
    path: C:\Program Files (x86)\Skype\Phone\Skype.exe
    company:  Skype Technologies S.A.
    product:  Skype
    filedesc: Skype
    filever:  6.7.32.102
    prodver:  6.7.0.0
SpotifyWebHelper.exe (2060)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyWebHelper.exe
    company:  Spotify Ltd
    product:  Spotify
    filedesc: SpotifyWebHelper
    filever:  0.9.7.16
    prodver:  0.9.7.16
Kies.exe (2640)
    path: C:\Program Files (x86)\Samsung\Kies\Kies.exe
    company:  Samsung
    product:  Kies
    filedesc: Kies
    filever:  1.0.0.1521
    prodver:  1.0.0.1521
Dropbox.exe (3040)
    path: C:\Users\Pelle\AppData\Roaming\Dropbox\bin\Dropbox.exe
    company:  Dropbox, Inc.
    product:  Dropbox
    filedesc: Dropbox
    filever:  2.4.11.0
    prodver:  1.0.0.1
uTorrent.exe (1344)
    path: C:\Users\Pelle\AppData\Roaming\uTorrent\uTorrent.exe
    company:  BitTorrent Inc.
    product:  �Torrent
    filedesc: �Torrent
    filever:  3.3.1.30017
    prodver:  3.3.1.30017
jusched.exe (2860)
    path: C:\Program Files (x86)\Common Files\Java\Java Update\jusched.exe
    company:  Oracle Corporation
    product:  Java(TM) Platform SE Auto Updater
    filedesc: Java(TM) Update Scheduler
    filever:  2.1.9.4
    prodver:  2.1.9.4
AdobeARM.exe (2176)
    path: C:\Program Files (x86)\Common Files\Adobe\ARM\1.0\AdobeARM.exe
    company:  Adobe Systems Incorporated
    product:  Adobe Reader and Acrobat Manager
    filedesc: Adobe Reader and Acrobat Manager
    filever:  1.7.4.0
    prodver:  1.7.4.0
KiesTrayAgent.exe (2692)
    path: C:\Program Files (x86)\Samsung\Kies\KiesTrayAgent.exe
    company:  Samsung Electronics Co., Ltd.
    product:  Kies TrayAgent
    filedesc: Kies TrayAgent Application
    filever:  2.0.0.143
    prodver:  0.0.0.23
ssh-agent.exe (940)
    path: C:\Users\Pelle\AppData\Local\GitHub\PortableGit_054f2e797ebafd44a30203088cd3d58663c627ef\bin\ssh-agent.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
ssh-agent.exe (6688)
    path: C:\Users\Pelle\AppData\Local\GitHub\PortableGit_054f2e797ebafd44a30203088cd3d58663c627ef\bin\ssh-agent.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
chrome.exe (7068)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (5940)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (6208)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (2596)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (5252)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (6172)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (1544)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (3648)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (3544)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
spotify.exe (4848)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\spotify.exe
    company:  Spotify Ltd
    product:  Spotify
    filedesc: Spotify
    filever:  0.9.7.16
    prodver:  0.9.7.16
SpotifyHelper.exe (1672)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
SpotifyHelper.exe (2620)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
SpotifyHelper.exe (1600)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
SpotifyHelper.exe (860)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
SpotifyHelper.exe (2492)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
adb.exe (6140)
    path: C:\Users\Pelle\Downloads\adt-bundle-windows-x86_64-20131030\adt-bundle-windows-x86_64-20131030\sdk\platform-tools\adb.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
DllHost.exe (7000)
    path: C:\Windows\SysWOW64\DllHost.exe
    company:  Microsoft Corporation
    product:  Microsoft� Windows� Operating System
    filedesc: COM Surrogate
    filever:  6.1.7600.16385
    prodver:  6.1.7600.16385
chrome.exe (3112)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
chrome.exe (2128)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
Unity.exe (7064)
    path: C:\Program Files (x86)\Unity\Editor\Unity.exe
    company:  Unity Technologies ApS
    product:  Unity
    filedesc: Unity Editor
    filever:  4.3.4.31067
    prodver:  4.3.4.31067
Powershell.exe (3336)
    path: C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Powershell.exe
    company:  Microsoft Corporation
    product:  Microsoft� Windows� Operating System
    filedesc: Windows PowerShell
    filever:  6.1.7600.16385
    prodver:  6.1.7600.16385
wish.exe (3976)
    path: C:\Users\Pelle\AppData\Local\GitHub\PortableGit_054f2e797ebafd44a30203088cd3d58663c627ef\bin\wish.exe
    company:  ActiveState Corporation
    product:  Tk 8.5 for Windows
    filedesc: Wish Application
    filever:  8.5.2.13
    prodver:  8.5.2.13
notepad++.exe (7512)
    path: C:\Program Files (x86)\Notepad++\notepad++.exe
    company:  Don HO don.h@free.fr
    product:  Notepad++
    filedesc: Notepad++ : a free (GNU) source code editor
    filever:  6.4.5.0
    prodver:  6.4.5.0
devenv.exe (7332)
    path: c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe
    company:  Microsoft Corporation
    product:  Microsoft� Visual Studio� 2010
    filedesc: Microsoft Visual Studio 2010
    filever:  10.0.30319.1
    prodver:  10.0.30319.1
SpotifyHelper.exe (7988)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
SpotifyHelper.exe (6036)
    path: C:\Users\Pelle\AppData\Roaming\Spotify\Data\SpotifyHelper.exe
    company:  
    product:  
    filedesc: 
    filever:  0.0.0.0
    prodver:  0.0.0.0
chrome.exe (1616)
    path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
    company:  Google Inc.
    product:  Google Chrome
    filedesc: Google Chrome
    filever:  33.0.1750.154
    prodver:  33.0.1750.154
UnityBugReporter.exe (9008)
    path: C:\Program Files (x86)\Unity\Editor\UnityBugReporter.exe
    company:  Unity Technologies ApS
    product:  Unity Bug Reporter
    filedesc: Unity Bug Reporter
    filever:  4.3.4.31067
    prodver:  4.3.4.31067
