.PNG files with embedded alpha in varying resolutions.


---------------


All files are Copyright 2012 Daniel Dye and In The Flesh Art

Terms Of Use:

All files can be used royalty free for personal and commercial work. The files must not be redistributed or sold without the express written consent of Daniel Dye or In The Flesh Art. 

Although not a requirement, if any of the files are used in any work, please send an email with a link to view the work to the contact details below, and if possible have a credit in your video or description.

If the files are used in a tutorial, you must credit Daniel Dye and In The Flesh Art and send an email with a link to the tutorial to the contact details below.

Email: daniel@inthefleshart.com
Web: www.inthefleshart.com
Youtube: www.youtube.com/coldsidedigital

Originally downloaded from: www.inthefleshart.com/freebies/cloud_smoke_textures.rar

Hope you enjoy!

Daniel Dye

Copyright 2012 Daniel Dye and In The Flesh Art.