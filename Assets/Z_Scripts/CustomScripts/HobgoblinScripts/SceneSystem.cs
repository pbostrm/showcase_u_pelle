﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class SceneSystem : MonoBehaviour
{
    static public SceneSystem currentActiveSceneSystem;

    public int ActiveFishInScene;
    public int WarmedFishCnt;
    public float recentlyFoundTimer;
    public void Awake()
    {
        currentActiveSceneSystem = this;
    }
    public void AddWarmedFish()
    {
        WarmedFishCnt++;
        recentlyFoundTimer = 0.8f;
    }
    public void OnGUI()
    {
        if (recentlyFoundTimer > 0.0f)
        {
            recentlyFoundTimer -= Mathf.Min(Time.deltaTime, recentlyFoundTimer);
        }
        GUI.Label(new Rect(100, 10, 200, 100), WarmedFishCnt.ToString() + " / " + ActiveFishInScene.ToString());
    }
}