﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GraphicControl : MonoBehaviour
{
    public Vector3 lastDelta;
    public Vector3 lastPosition;
    public Vector3 targetDirection;
    public float _Warmth;
    public float Warmth
    {
        get
        {
            return _Warmth;
        }
        set
        {
            if (Warmth != value)
            {
                if (value >= 3.0f)
                {
                    warmthColor = UtilityColor.hsv2rgb(0.5f, 1.0f, 1.0f);
                }
                else
                {
                    warmthColor = UtilityColor.hsv2rgb((value / 2.0f) * 0.4f, 1.0f, 1.0f);

                }
                _Warmth = value;
            }
        }
    }
    public Color warmthColor;
    Animator animator;
    public float speed = 1.0f;
    public Quaternion testQuat;

    public List<Transform> TailTransforms;

    public float Zrot;
    GraphicInfo graphicInfo;
    float randomTimer;
    public void Awake()
    {
        animator = GetComponent<Animator>();
        graphicInfo = GetComponent<GraphicInfo>();
        lastPosition = transform.position;
        //animator.speed = 0.0f;
        //randomTimer = UnityEngine.Random.Range(0.0f, 1.5f);
    }
   /* public void OnDrawGizmos()
    {
        //Gizmos.DrawLine(transform.position, transform.position + targetDirection);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward*10.0f);
        Gizmos.color = Color.red;

        //Vector3 b = bbbb + ((lastForward - bbbb) / Time.deltaTime);
        Gizmos.DrawLine(transform.position, transform.position + targetDirection * 10.0f);
    }
    */
    Vector3 bbbb;
    Vector3 lastForward;
    public Quaternion qt;

    float timer;
    public void LateUpdate()
    {

        if (animator != null)
        {
            
            animator.speed = speed*0.5f;

            if (graphicInfo != null)
            {

                //Vector3 delta = lastDelta - (transform.position-lastPosition).normalized;

                //graphicInfo.ModifyBones(delta);
                //lastDelta = (transform.position - lastPosition).normalized;
                if (timer < 0.0f)
                {
                    timer += 1.0f;
                    //bbbb = lastForward;
                    //lastForward = (transform.position - lastPosition).normalized;
                    //lastPosition = transform.position;

                }
                timer-=Time.deltaTime;

                //qt = Quaternion.FromToRotation(transform.position - (transform.position + targetDirection), transform.position - (transform.position + transform.forward));

                //Vector3 b = bbbb + ((lastForward - bbbb) / Time.deltaTime);
                //qt = Quaternion.FromToRotation(bbbb, lastForward);
                //qt.w *= ;
                //graphicInfo.ModifyBones(qt);

            }
            //lastPosition = transform.position;
            
        }
    }
}