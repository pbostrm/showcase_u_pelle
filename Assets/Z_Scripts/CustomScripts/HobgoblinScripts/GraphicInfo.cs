﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GraphicInfo :MonoBehaviour
{
    //public Vector3
    public List<ModifiableBone> ModifiableBones;

    Transform tailAnchor;
    Transform tailFollow;

    public void Awake()
    {
        //tailFollow = new GameObject("TailFollow").transform;
        //tailFollow = new GameObject("TailAnchor").transform;

        for (int i = 0; i < ModifiableBones.Count; i++)
        {

            ModifiableBones[i].Init();

        }

    }

    public void Update()
    {
       
        for (int i = 0; i < ModifiableBones.Count; i++)
        {

            ModifiableBones[i].Update(transform);

        }


    }
    public void ModifyBones(Quaternion direction)
    {
        if (ModifiableBones != null)
        {
            for (int i = 0; i < ModifiableBones.Count; i++)
            {
                //Vector3 localEulerAngles = ModifiableBones[i].BoneTransform.localEulerAngles;
                //ModifiableBones[i].BoneTransform.LookAt(tailFollow);
                //transform.loo

                //ModifiableBones[i].BoneTransform.localRotation *= direction;
                //localEulerAngles = direction * ModifiableBones[i].ModifierWeight;
                //ModifiableBones[i].BoneTransform.localEulerAngles = localEulerAngles;
            }
        }
    }
}

[Serializable]
class ModifiableBone
{
    //Transform tailFollow;
    Vector3 tailFollow;
    public float TailDistance;
    public Transform BoneTransform;
    //public Vector3 chainPoint;
    //public float ModifierWeight = 1.0f;
    public ModifiableBone()
    {

    }
    public ModifiableBone(Transform input)
    {
        BoneTransform = input;
        //ModifierWeight = 1.0f;
    }

    public void Init()
    {
        return;
       /* GameObject g = GameObject.Find("_TailLinks");
        if (g == null)
        {
            g = new GameObject("_TailLinks");
        }
        tailFollow = new GameObject("TailFollow").transform;
        tailFollow.parent = g.transform;*/
    }
    public void Update(Transform transform)
    {
        Vector3 dir = (BoneTransform.position - tailFollow);

        //transform.forward = dir.normalized;

        dir = dir.normalized *TailDistance;
        tailFollow = BoneTransform.position - dir;

       // Debug.DrawLine(BoneTransform.position, tailFollow);

        BoneTransform.LookAt(tailFollow,transform.up);
        Debug.DrawLine(transform.position, tailFollow);
        
        //BoneTransform.LookAt(tailFollow);

        //dir = BoneTransform.localEulerAngles;
        //dir.z = 0.0f;
        //BoneTransform.localEulerAngles = dir;
        

        
    }
}

