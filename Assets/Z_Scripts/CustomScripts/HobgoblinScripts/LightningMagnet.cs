﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class LightningMagnet : MonoBehaviour
{
    public Transform source;
    LineRenderer lineRenderer;
    float boltCycler;
    float boltTime = 1.0f;
    float boltCD;

    bool destroyThis;
    public void Awake()
    {
        lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, transform.position);
        lineRenderer.SetWidth(0.4f, 0.0f);
        lineRenderer.material = (Material)Instantiate(Resources.Load("Colorbolt"));
        boltCycler = boltTime;
    }


    void Update()
    {
        if (source != null)
        {
            lineRenderer.SetPosition(1, source.position);

        }
        if (boltCD <= 0.0f)
        {
            lineRenderer.enabled = true;

            if (boltCycler < 0.0f)
            {
                //lineRenderer.SetPosition(0, transform.position);
                boltCycler = boltTime;
                boltCD = UnityEngine.Random.Range(0.1f,0.8f);
                if (destroyThis)
                {
                    Destroy(this);
                }

            }
            lineRenderer.SetPosition(0, transform.position);

            Vector2 offset = lineRenderer.material.mainTextureOffset;
            /*offset.y = ((int)((boltCycler / boltTime) * 16.0f)) * (1.0f / 16f);
            offset.x = (((int)((boltCycler / boltTime)*48)%16.0f)) * (1.0f / 3f);*/

            offset.y = ((((int)((boltCycler / boltTime) * 48) % 16.0f)) *(1.0f / 16f));
            //offset.x = ((((int)((boltCycler / boltTime) * 48)/16.0f)) * (1.0f / 3f));
            offset.x = (int)((1.0f-(boltCycler / boltTime)) * 3.0f) * 0.3333f;
            lineRenderer.material.mainTextureOffset = offset;
            boltCycler -= Time.deltaTime;
        }
        else
        {
            lineRenderer.enabled = false;
        }
        boltCD -= Time.deltaTime;


    }
    public void QueueDestroy()
    {
        destroyThis = true;
    }
    public void OnDestroy()
    {
        Destroy(lineRenderer);
    }
}
