﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class DSTProximityAgent : sBehaviour
{
    public static DSTNode nodeRoot;
    public float Radius = 1.0f;
    DSTNode proximityLeaf;
    public void Awake()
    {
        if (nodeRoot == null)
        {
            nodeRoot = new DSTNode(true);

        }
   
        proximityLeaf = new DSTNode(transform.position,Radius, true);
        nodeRoot.Insert(proximityLeaf);     

    }
}