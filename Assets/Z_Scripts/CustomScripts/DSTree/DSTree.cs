﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class DSTNode
{
    //Disgusting Sphere Tree, based upon the SS+Tree
    public DSTNode parent;

    public Vector3 position;
    public float radius = 2;

    List<DSTNode> subNodes;
    List<DSTNode> leaves;
    List<DSTNode> balancedNodes;

    int maxLeaves =5;
    bool endBranch;
    bool leaf;

    public DSTNode()
    {
        GizmoColor = UtilityColor.GetRandomColor();
        DSTreeDrawer.AddDrawer(this);
    }
    public DSTNode(Color col)
    {
        GizmoColor = col;
        DSTreeDrawer.AddDrawer(this);
    }
    public DSTNode(bool root): this()
    {
        GizmoColor = Color.black;
        AddNode(new DSTNode(Color.red));
        AddNode(new DSTNode(Color.green));
        AddNode(new DSTNode(Color.blue));
    }
    public DSTNode(Vector3 pos,float r,bool l): this()
    {
        leaf = l;
        position = pos;
        radius = r;
    }
     ~DSTNode()
    {
        DSTreeDrawer.RemoveDrawer(this);
    }

    public void AddNode(DSTNode node)
    {
        
        if (subNodes == null)
        {
            subNodes = new List<DSTNode>();
        }
        subNodes.Add(node);
        node.parent = this;

        if (node.leaf)
        {
            DebugOutput.Shout("penis leaf");
        }
            
        

    }
    public void AddLeaf(DSTNode node)
    {
        if (subNodes != null && subNodes.Count > 0)
        {
            DebugOutput.Shout("there is subnodes already here.");
        }
        else
        {
            if (leaves == null)
            {
                leaves = new List<DSTNode>();
            }
            leaves.Add(node);
            node.parent = this;

        }
        
        

    }

    public void Insert(DSTNode node)
    {
        if (leaf)
        {
            DebugOutput.Shout("leaf penis");
            return;
        }
        /*if (subNodes == null )
        {
            if (subNodes == null)
            {
                subNodes = new List<DSTNode>();
                endBranch = true;
            }
            
            //insert right here
        }*/
        if (subNodes == null)
        {
            subNodes = new List<DSTNode>();
        }

        if (subNodes != null)
        {
            if (subNodes.Count > 0)
            { 
            // there is subnodes!
                bool inserted = false;
                foreach (var branch in subNodes)
                {
                    if (branch.isPointInside(node.position))
                    {
                        branch.Insert(node);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted)
                {

                    //find closest with least expansion; 
                    DSTNode smallestGrowthNode = FindLeastExpanded(node);

                    if (smallestGrowthNode != null)
                    {
                        smallestGrowthNode.Insert(node);
                    }
                    else
                    {
                        DebugOutput.Shout("Failed to insert node some where");
                    }

                }
            }
            else
            {
                InsertLeaf(node);

            }
        }
        

        Resize();
            
        
        //does Node have Subnodes
    }
    public void InsertLeaf(DSTNode node)
    {
        
        if (leaves == null)
        {
            leaves = new List<DSTNode>();
        }
        AddLeaf(node);

        if (leaves.Count >= maxLeaves)
        {

            if (parent != null)
            {
                //first check if we can rebalance siblings
                if (CheckParentSpace())
                {
                    //DebugOutput.Shout("there is space in siblings, doing KMeanSplit");
                    parent.KMeanSplit();
                    
                }
                else
                {
                    // if we cant rebalance we have to split current
                    //DebugOutput.Shout("Expanding this node instead");

                    ExpandThisNode();
                }
            }
            else
            {
                DebugOutput.Shout("Parent is null");
            }

            //do that split yo
        }

    }
    public void ExpandThisNode()
    {
        
        DSTNode topSubNode = new DSTNode(Color.magenta);
        //topSubNode.parent = parent;
        parent.Swap(this, topSubNode);

        GizmoColor = Color.red;
        topSubNode.AddNode(this);
        topSubNode.AddNode(new DSTNode(Color.green));
        topSubNode.AddNode(new DSTNode(Color.blue));
        topSubNode.KMeanSplit();
        topSubNode.Resize();
    }
    public DSTNode FindLeastExpanded(DSTNode node)
    {
        float minAreaGrowth = 0.0f;
        float radiusSquared;
        DSTNode smallestGrowthNode = null;
        foreach (var branch in subNodes)
        {
            if (branch.subNodes == null || branch.subNodes.Count == 0)
            {
                return branch;
            }
            else
            {
                radiusSquared = (branch.position - node.position).sqrMagnitude;
                float areaGrowth = (3.14f * radiusSquared) - (3.14f * (radius * radius));
                if (minAreaGrowth == 0.0f || areaGrowth <= minAreaGrowth)
                {
                    minAreaGrowth = areaGrowth;
                    smallestGrowthNode = branch;
                    //parent = parentSub;
                }
            }

        }
        return smallestGrowthNode;
    }
    public bool isPointInside(Vector3 point)
    {
        if ((position - point).sqrMagnitude <= radius * radius)
        {
            return true;
        }
        return false;
    }
    public bool CheckParentSpace()
    {
        
        foreach (var subNode in parent.subNodes)
        {
            if (subNode.leaves == null || subNode.leaves.Count < subNode.maxLeaves)
            {
                return true;
            }
        }
        return false;
    }
    public void Swap(DSTNode oldNode, DSTNode newNode)
    {
        if(subNodes !=null)
        {
            subNodes.Remove(oldNode);
            AddNode(newNode);
            newNode.position = oldNode.position;
        }
    }
    static bool KMeanDone;

    public bool KMeanSplit()
    {
        //K-Mean Split
        
        if (subNodes == null || subNodes.Count != 3)
        {
            DebugOutput.Shout("Aborting KMeanInit, no subnodes");
            return false;
        }

        if(subNodes.Count == 3)
        {
            if (leaves != null && leaves.Count > 0)
            {
                DebugOutput.Shout("We got some leaves here that shouldnt be");
                foreach (var leaf in leaves)
                {
                    subNodes[0].AddLeaf(leaf);

                }
                leaves.Clear();
            }
            KMeanInit();
            KMeanDone = true;
            for (int i = 0; i < 3; i++)
            {
                //DebugOutput.Shout("Balancing KMean nr: "+i);

                KMeanBalance();
            }

            //DebugOutput.Shout("Resizing KMEANSPLIT");

            for (int i = 0; i < subNodes.Count; i++)
            {
                DSTNode sub = subNodes[i];
                if (sub.leaves == null)
                {
                    DebugOutput.Shout("penis sub leaves");
                }
                else
                {
                    if (sub.leaves.Count >= sub.maxLeaves)
                    {
                        //DebugOutput.Shout("this sub still have too many leaves");
                        sub.ExpandThisNode();
                    }
                    //sub.Resize();
                }
                
                
            }
        }
        // List<DSTNode> targets = new List<DSTNode>();
        return true;
    }

    void KMeanInit()
    {
        //find first random
        foreach (var branch in subNodes)
        {
            // take a leaf from the other guys.
            if (branch.leaves == null || branch.leaves.Count == 0)
            {
                foreach (var branch2 in subNodes)
                {
                    if (branch2.leaves == null || branch2.leaves.Count > 1)
                    {
                        branch.AddLeaf(branch2.leaves[0]);
                        branch.Resize();
                        branch2.leaves.RemoveAt(0);
                        break;
                    }
                }
        
            }
        }
        
        DSTNode KMeanBranch = subNodes[0];

        if (KMeanBranch.leaves != null && KMeanBranch.leaves.Count > 0)
        {
            KMeanBranch.position = KMeanBranch.leaves[0].position;
            

            //DebugOutput.Shout("Got First Random Centroid!");
        }
        //find second (furthest away from firsT)

        KMeanBranch = subNodes[1];

        if (KMeanDone)
        Debug.DrawLine(subNodes[0].position - Vector3.forward * 3, subNodes[0].position + Vector3.forward * 3, Color.white, 100);

        float MaxDist = 0;
        foreach (var branch in subNodes)
        {

            DSTNode furthestDSTNode = branch.GetFurthestDistance(subNodes[0].position);
            if (furthestDSTNode == null)
            {
                DebugOutput.Shout("prutt 2!");
            }
            else
            {
                float cDist = (subNodes[0].position - furthestDSTNode.position).sqrMagnitude;
                if (cDist >= MaxDist)
                {
                    MaxDist = cDist;
                    //Debug.DrawLine(KMeanBranch.position - Vector3.forward * 3, KMeanBranch.position + Vector3.forward * 3, Color.magenta, 100);
                    KMeanBranch.position = furthestDSTNode.position;
                    //Debug.DrawLine(KMeanBranch.position - Vector3.one * 3, KMeanBranch.position + Vector3.one * 3, Color.cyan, 100);
                }
            }

        }
        if (KMeanDone)
        Debug.DrawLine(KMeanBranch.position - Vector3.one * 3, KMeanBranch.position + Vector3.one * 3, Color.cyan, 100);

        //DebugOutput.Shout("Got Second Furthest Centroid!");


        //find third... 
        MaxDist = 0;
        foreach (var branch in subNodes)
        {

            DSTNode furthestDSTNode = branch.GetFurthestDistance(subNodes[0].position, subNodes[1].position);
            if (furthestDSTNode == null)
            {
                DebugOutput.Shout("prutt 3!");
            }
            else
            {
                float cDist = Mathf.Min((subNodes[0].position - furthestDSTNode.position).sqrMagnitude, (subNodes[1].position - furthestDSTNode.position).sqrMagnitude);
                if (cDist >= MaxDist)
                {
                    MaxDist = cDist;
                    subNodes[2].position = furthestDSTNode.position;
                    
                }
            }

        }
        if (KMeanDone)
            Debug.DrawLine(subNodes[2].position - Vector3.up * 3, subNodes[2].position + Vector3.up * 3, Color.magenta, 100);

        //DebugOutput.Shout("Got third Furthest Centroid!");
    }
    void KMeanBalance()
    {
        foreach (var sub in subNodes)
        {
            if (sub.leaves != null)
            {
                for (int i = sub.leaves.Count - 1; i > 0; i--)
                {
                    sub.leaves[i].FindClosestParent();
                }
                
            }
        }
        foreach (var sub in subNodes)
        {
            sub.MergeBalancedLeaves();
            sub.KMeanFindCentroid();
        }
    }
    public void KMeanFindCentroid()
    {
        if (subNodes == null)
        {
            return;
        }
        //k find stuff magic this is just placeholder logic
        Vector3 v = new Vector3();

        foreach (var subNode in subNodes)
        {
            v += subNode.position;
        }
        v = v / subNodes.Count;
        position = v;

    }
    public void FindClosestParent()
    {
        float SQRdist;
        float minSQRdist = 0.0f;
        DSTNode newParent = parent;
        if (parent.parent != null)
        {
            foreach (var parentSub in parent.parent.subNodes)
            {
                SQRdist = (parentSub.position - position).sqrMagnitude;
                if (minSQRdist == 0.0f || SQRdist <= minSQRdist)
                {
                    minSQRdist = SQRdist;
                    newParent = parentSub;
                }
            }
        }
        if (newParent != parent)
        {
            parent.RemoveLeaf(this);
            newParent.AddBalancedNode(this);
        }

    }
    public void AddBalancedNode(DSTNode node)
    {
        if (balancedNodes == null)
        {
            balancedNodes = new List<DSTNode>();
        }
        if (!balancedNodes.Contains(node))
        {
            balancedNodes.Add(node);
        }
    }
    public void MergeBalancedLeaves()
    {
        if (balancedNodes == null)
        {
            return;
        }
        //DebugOutput.Shout("found " + balancedNodes.Count);

        foreach (var balancedNode in balancedNodes)
        {
            //balancedNode.parent.RemoveLeaf(balancedNode);
            balancedNode.parent = this;
        }
        leaves.AddRange(balancedNodes);
        balancedNodes.Clear();
    }
    public void RemoveLeaf(DSTNode node)
    {
        leaves.Remove(node);
        node.parent = null;
    }

    public DSTNode GetFurthestDistance(Vector3 pointA, Vector3 pointB)
    {
        if (leaves == null || leaves.Count == 0)
        {
            return null;
        }

        float maxDistSQR = 0;


        DSTNode furthestNode = null;
        foreach (var leaf in leaves)
        {
            float DistSQR = Mathf.Min((pointA - leaf.position).sqrMagnitude, (pointB - leaf.position).sqrMagnitude);
            if (DistSQR>= maxDistSQR)
            {
                maxDistSQR = DistSQR;
                furthestNode = leaf;
            }
        }
        return furthestNode;
    }
    public DSTNode GetFurthestDistance(Vector3 point)
    {

        List<DSTNode> nodeSource;
        if (leaves != null && leaves.Count > 0)
        {
            nodeSource = leaves;
        }
        else
        {
            nodeSource = subNodes;
        }


        if (nodeSource == null || nodeSource.Count == 0)
        {
            return null;
        }

        float maxDistSQR = 0;

        DSTNode furthestNode = null;
        foreach (var node in nodeSource)
        {
            float DistSQR = (point - node.position).sqrMagnitude;
            if (DistSQR >= maxDistSQR)
            {
                maxDistSQR = DistSQR;
                furthestNode = node;
            }
        }
        if (furthestNode == null)
        {
            furthestNode = this; 
        }
        return furthestNode;
    }

    void Resize()
    {
        //Resize2();
        KMeanResize();
        return;
        //DebugOutput.Shout("Resizing");
        if (subNodes == null)
        {
            return;
        }
        //k find stuff magic this is just placeholder logic
        Vector3 v = new Vector3();
        
        foreach (var subNode in subNodes)
        {
            v += subNode.position;
        }
        v = v / subNodes.Count;
        position = v;

        float maxdistanceSQR = 0;
        foreach (var subNode in subNodes)
        {
            float sqrDist = (position - subNode.position).sqrMagnitude;
            if (sqrDist > maxdistanceSQR)
            {
                v = subNode.position;
                maxdistanceSQR = sqrDist;
            }
        }
        radius = (position - v).magnitude;
        
    }
    void Resize2()
    {
        //DebugOutput.Shout("Resizing2");

        List<DSTNode> resizeSource;
        if (leaves != null && leaves.Count > 0)
        {
            resizeSource = leaves;
        }
        else
        {
            resizeSource = subNodes;
        }

        if (resizeSource == null || resizeSource.Count == 0)
        {
            DebugOutput.Shout("subnodes = sheit");

            return;
        }
        if (resizeSource.Count == 1)
        {
            position = resizeSource[0].position;
            radius = resizeSource[0].radius * 1.1f;
        }
        else
        {
            Vector3 lastPoint = resizeSource[0].position;
            Vector3 oldPos, v1, v2;
            v1 = Vector3.zero;
            v2 = Vector3.zero;
            position = (lastPoint + resizeSource[1].position) * 0.5f;
            for (int i = 2; i < resizeSource.Count; i++)
            {
                v1 = (position - resizeSource[i].position);
                if (v1.sqrMagnitude > v2.sqrMagnitude)
                {
                    oldPos = position;
                    v2 = position - lastPoint;

                    position = ((position + resizeSource[i].position) * 0.5f) - (v2 * 0.5f);
                    Debug.DrawLine(oldPos, position, GizmoColor, 1000.0f);
                    lastPoint = resizeSource[i].position;
                }
                
            }
            radius = (position-lastPoint).magnitude;
        }
        
    }
    void KMeanResize()
    {
        List<DSTNode> nodeSource;
        if (leaves != null && leaves.Count > 0)
        {
            nodeSource = leaves;
        }
        else
        {
            nodeSource = subNodes;
        }

        if (nodeSource == null || nodeSource.Count == 0)
        {
            DebugOutput.Shout("subnodes = sheit");

            return;
        }

        if (nodeSource.Count == 1)
        {
            position = nodeSource[0].position;
            radius = nodeSource[0].radius * 1.1f;
        }
        else
        {
            //init center;


            Vector3 v = new Vector3();

            foreach (var subNode in nodeSource)
            {
                v += subNode.position;
            }
            v = v / nodeSource.Count;
            position = v;


            for (int i = 1; i < 5 && i < nodeSource.Count; i++)
            {
                v = GetFurthestDistance(position).position;
                position = Vector3.Lerp(position,v, (i * 0.38f));
            }
            radius = (position - v).magnitude;
        }
    }


    Color GizmoColor;
    public void OnGizmos()
    {
        Gizmos.color = GizmoColor;
        if (parent == null)
        {
            Gizmos.DrawWireSphere(position, radius + 0.5f);
           // Gizmos.DrawWireSphere(position, radius + 1f);

        }
        else
        {
            Vector3 newpos = position;
            newpos.y = parent.position.y - 30.0f;
            position = newpos;
        }

        Gizmos.DrawWireSphere(position, radius);

        Gizmos.DrawLine(position + Vector3.left, position + Vector3.right);
        Gizmos.DrawLine(position + Vector3.up, position + Vector3.down);
        Gizmos.DrawLine(position + Vector3.forward, position + Vector3.back);

        if (leaves != null)
        {
            foreach (var sub in leaves)
            {
                Gizmos.DrawLine(position, sub.position);
                //Gizmos.DrawWireCube(sub.position, Vector3.one * sub.radius * 2);
            }
        }
        if (subNodes != null)
        {
            foreach (var sub in subNodes)
            {
                Gizmos.DrawLine(position, sub.position);
        //        Gizmos.DrawWireCube(sub.position, Vector3.one * sub.radius * 2);
        //        Gizmos.DrawWireCube(sub.position, (Vector3.one * sub.radius * 2)+(Vector3.one*0.1f));
            }
        }
        /*if (parent != null)
        {
            Gizmos.color = parent.GizmoColor;

            Gizmos.DrawLine(position, parent.position);
            Gizmos.DrawWireCube(position,Vector3.one*radius*2);

        }*/
    }
    public void DrawSelectedGizmo()
    {
        Gizmos.color = GizmoColor;

        Gizmos.color = GizmoColor;
        if (parent == null)
        {
            // Gizmos.DrawWireSphere(position, radius + 0.5f);
            // Gizmos.DrawWireSphere(position, radius + 1f);

        }
        else
        {
            Vector3 newpos = position;
            newpos.y = parent.position.y - 30.0f;
            position = newpos;
        }
        Gizmos.DrawWireSphere(position, 1.0f);

        if (parent != null)
        {
            Gizmos.DrawLine(position, parent.position);
            parent.DrawSelectedGizmo();
        }
    }
}
