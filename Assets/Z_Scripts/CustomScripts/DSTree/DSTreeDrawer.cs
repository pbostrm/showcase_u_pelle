﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class DSTreeDrawer : MonoBehaviour
{

    static public List<DSTNode> nodes;

    static public void AddDrawer(DSTNode node)
    {
        if (nodes == null)
        {
            nodes = new List<DSTNode>();
        }
        if (!nodes.Contains(node))
        {
            nodes.Add(node);
        }
    }
    static public void RemoveDrawer(DSTNode node)
    {
        if (nodes == null)
        {
            return;
        }
        if (nodes.Contains(node))
        {
            nodes.Remove(node);
        }
    }
    public void OnDrawGizmos()
    {
        if (nodes == null)
        {
            return;
        }
        Gizmos.color = Color.yellow;

        for (int i = nodes.Count - 1; i >= 0; i--)
        {
            if (nodes[i] == null)
            {
                nodes.RemoveAt(i);
            }
            else
            {
                nodes[i].OnGizmos();            
            }
        }

    }
}