﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DSpawnArea))]
class DSpawnAreaEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DSpawnArea t = (DSpawnArea)target;
        GUILayout.Label("Banana");

        if (!t.BuildingArea)
        {
            if (GUILayout.Button("Build Spawn Area"))
            {
                t.BuildSpawnArea();
            }
        }
        else
        {
            GUI.contentColor = Color.red;
            GUILayout.Label("Building Spawn Area ");
            GUI.contentColor = Color.white;

        }

        DrawDefaultInspector();
    }

    
}
