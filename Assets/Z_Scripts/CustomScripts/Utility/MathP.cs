﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
class MathP
{
    static public Vector3 RotateAround(Vector3 point,Vector3 pivot,Quaternion rotation)
    {
        return (rotation *(point-pivot))+pivot;
    }
}