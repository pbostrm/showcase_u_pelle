﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class StaticRotation : MonoBehaviour
{
    public float RPS;
    public Vector3 axis;

    public void Update()
    {
        transform.Rotate(axis, RPS * Time.deltaTime);
       
    }
}
