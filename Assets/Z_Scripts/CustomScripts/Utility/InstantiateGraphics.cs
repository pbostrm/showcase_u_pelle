﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class InstantiateGraphics : MonoBehaviour
{
    public GameObject graphics;

    public void Awake()
    {
        if (graphics != null)
        {
            GameObject g = (GameObject)Instantiate(graphics);
            g.transform.parent = transform;
            g.transform.localPosition = Vector3.zero;
            g.transform.localEulerAngles = Vector3.zero;

            
        }
        Destroy(this);
    }
}