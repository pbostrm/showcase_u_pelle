﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class UtilityColor
{
    public static Color GetRandomColor()
    {


        return hsv2rgb(UnityEngine.Random.Range(0.0f, 1.0f), 1.0f, 1.0f);

    }
    /*public static Color GetRandomColor()
    {


        return hsv2rgb(UnityEngine.Random.Range(0.0f, 1.0f), 1.0f, 1.0f);

    }*/
    static public Color hsv2rgb(float h, float s, float v)
    {
        h = (h % 1 + 1) % 1; // wrap hue

        int i = Mathf.FloorToInt(h * 6);
        float f = h * 6 - i;
        float p = v * (1 - s);
        float q = v * (1 - s * f);
        float t = v * (1 - s * (1 - f));


        switch (i)
        {
            case 0:
                return new Color(v, t, p);
            case 1:
                return new Color(q, v, p);

            case 2:
                return new Color(p, v, t);

            case 3:
                return new Color(p, q, v);

            case 4:
                return new Color(t, p, v);

            case 5:
                return new Color(v, p, q);
        }
        return Color.black;
    }
}