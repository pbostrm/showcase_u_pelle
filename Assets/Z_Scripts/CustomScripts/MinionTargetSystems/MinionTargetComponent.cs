﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
class MinionTargetComponent : MonoBehaviour
{
    public bool isFocused;
    MinionTarget thisTarget;
    public bool refreshed;
    int queuedMinions;
    public void Awake()
    {
        thisTarget = new MinionTarget();
        thisTarget.point = transform.position;
        rigidbody.isKinematic = true;
        collider.isTrigger = true;

        //gameObject.layer = LayerMask.
    }
    public void Update()
    {
        if (isFocused)
        {
            transform.Rotate(Vector3.up, 30.0f * Time.deltaTime);

        }
    }
    public void preSyncModule(GoldFishBase group)
    {
        if (thisTarget == null)
        {
            return;
        }
       // DebugOutput.Shout("Suck! registeredbehaviour " + thisTarget.RegisteredBehaviour.ToString());

        for (int i = 0; i < group.totalFish; i++)
        {
            
            if (group.UpdateBehaviourID[i] == 3)
            {
                group.UpdateBehaviourID[i] = thisTarget.RegisteredBehaviour;
                group.minionTarget[i] = thisTarget.RegisteredIndex;
                queuedMinions--;
            }
            if (queuedMinions <= 0)
            {
                break;
            }
        }
        queuedMinions = 0;
    }
    public void AddMinion(int index, GoldFishBase group)
    {
        thisTarget.RegisteredIndex = (byte)group.RegisterMinionTarget(thisTarget);
        //thisTarget.RegisteredBehaviour = (byte)group.RegisterMinionTargetBehaviour(group.BaseTargetBehaviour);
        thisTarget.RegisteredBehaviour = (byte)group.RegisterMinionTargetBehaviour(getCustomBehaviour(group));
        group.RegisterPresync(preSyncModule);
        queuedMinions++;
    }
    protected virtual GoldFishBase.UpdateBehaviour getCustomBehaviour(GoldFishBase group)
    {
        return group.BaseTargetBehaviour;
    }
}

class MinionTarget
{
    public Vector3 point;

    public byte RegisteredIndex;
    public byte RegisteredBehaviour;
    public MinionTarget()
    {

    }
}
partial class GoldFishBase : DFish
{
    List<MinionTarget> minionTargets;
    
    List<PresyncModule> presyncModules;
    bool gotPresyncModules;
    public delegate void PresyncModule(GoldFishBase group);
    public void RegisterPresync(PresyncModule module)
    {
        if (presyncModules == null)
        {
            presyncModules = new List<PresyncModule>();
        }
        if (!presyncModules.Contains(module))
        {
            presyncModules.Add(module);
        }
        gotPresyncModules = true;
    }
    public int RegisterMinionTarget(MinionTarget target)
    {
        if (minionTargets == null)
        {
            minionTargets = new List<MinionTarget>();
        }

        if (minionTargets.Contains(target))
        {
            for (int i = 0; i < minionTargets.Count; i++)
            {
                if (minionTargets[i] == target)
                {
                    return i;
                }
            }
            return -1;
        }
        else
        {
            minionTargets.Add(target);
            return minionTargets.Count - 1;
        }
    }
    public int RegisterMinionTargetBehaviour(UpdateBehaviour behaviour)
    {
        for (int i = 7; i < updateBehaviours.Length; i++)
        {
            if (updateBehaviours[i] == behaviour)
            {
                return i;
            }
        }
        for (int i = 7; i < updateBehaviours.Length; i++)
        {
            if (updateBehaviours[i] == null)
            {
                updateBehaviours[i] = behaviour;
                return i;
            }
        }
        return -1;
    }
    public void BaseTargetBehaviour(int index)
    {
        groupingVector = Vector3.zero;


        MakeDelta(index,minionTargets[minionTarget[index]].point);
        if (MoveToTarget(index, 7.0f))
        {
            groupingVector = delta.normalized;
        }

        CombineRepelGroupMatching(index);
     
    }

    void CombineRepelGroupMatching(int index)
    {
        workTargetVectors[index] = (workRepelVectors[index]) +
                             groupingVector +
                             (matchingVector * 0.5f);
    }
    bool MoveToTarget(int index, float radius)
    {
        
        if (delta.sqrMagnitude > (radius*2.0f)*radius)
        {
            targetSpeed[index] = speed * 2.0f;
            return true;
        }
        else
        {
            targetSpeed[index] = speed;
            return false;
        }
    }
    void MakeDelta(int index,Vector3 point)
    {
        delta = (point - localTree.points[index]);
    }
    Vector3 Encircle(Vector3 point,Vector3 pivot,Vector3 axis,float distance,float direction)
    {
        return point - MathP.RotateAround(pivot-(delta.normalized*distance), pivot, Quaternion.AngleAxis(direction,axis));
    }

}
