﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class MTmoveObject : MinionTargetComponent
{
    protected override DFish.UpdateBehaviour getCustomBehaviour(GoldFishBase group)
    {
        return group.MTMoveObjectBehaviour;
    }
}

partial class GoldFishBase : DFish
{
    public void MTMoveObjectBehaviour(int index)
    {
        groupingVector = Vector3.zero;


        MakeDelta(index, minionTargets[minionTarget[index]].point);
        if (MoveToTarget(index, 10.0f))
        {
            groupingVector = delta.normalized;
        }
        else
        {
            groupingVector = Encircle(localTree.points[index], minionTargets[minionTarget[index]].point, Vector3.up, 5.0f, 30.0f).normalized;
        }

        CombineRepelGroupMatching(index);

    }
}
