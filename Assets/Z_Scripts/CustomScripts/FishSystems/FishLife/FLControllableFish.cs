﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FLControllableFish : PMonoBehaviour
{
    FLFishBase parentSystem;
    public int psIndex;

    Transform cameraFollowerAnchor;
    float cameraDistance = 8.0f;
    void Awake()
    {
        DebugOutput.Shout("FLControllableFish Added to " +gameObject.name );
        //gameObject.AddComponent<ControllableFish>();
        CameraFollower cf = Camera.main.gameObject.GetComponent<CameraFollower>();
        if (cf == null)
        {
            cf = Camera.main.gameObject.AddComponent<CameraFollower>();
        }

        if (cf != null)
        {
            cameraFollowerAnchor = GetSetTransform("_cameraFollowerAnchor");
                
            cf.target = cameraFollowerAnchor;
            cf.lookAtTarget = transform;
            cf.distance = 1.0f;
        }
    }
    public void ReleaseControl()
    {
        DebugOutput.Shout("Releasing Control from: " + gameObject.name);
    }
    void Update()
    {

        if (cameraFollowerAnchor != null)
        {
            //rotation
            if (Input.mousePresent)
            {
                Vector3 cameraInput = new Vector3();
                cameraInput.x = (((Input.mousePosition.x / Screen.width) * 2) - 1.0f);
                cameraInput.y = (((Input.mousePosition.y / Screen.height) * 2) - 1.0f);

                cameraFollowerAnchor.RotateAround(transform.position, Vector3.up, 100 * cameraInput.x * Time.deltaTime);
                cameraFollowerAnchor.RotateAround(transform.position, -Camera.main.transform.right, 60 * cameraInput.y * Time.deltaTime);

            }



            //positioning
            Vector3 pos = cameraFollowerAnchor.position;
            pos.y = Mathf.Clamp(pos.y, transform.position.y - (cameraDistance * 0.5f), transform.position.y + (cameraDistance * 0.5f));

            Vector3 dir = (transform.position - cameraFollowerAnchor.position);

            //cameraFollowerAnchor.forward = Vector3.RotateTowards(cameraFollowerAnchor.forward, dir.normalized, 0.3f * Time.deltaTime, 1.0f);

            if (dir.sqrMagnitude > (cameraDistance * 2.0f) * (cameraDistance * 2.0f))
            {
                pos = transform.position - (dir.normalized * (cameraDistance * 2.0f));
            }
            else
            {
                dir = cameraFollowerAnchor.position - (transform.position + dir.normalized * cameraDistance);
                pos += dir.normalized * Time.deltaTime * 2.0f;
            }
            
            
            cameraFollowerAnchor.position = pos;
        }
    }
}