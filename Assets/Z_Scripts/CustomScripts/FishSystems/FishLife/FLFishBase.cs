﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class FLFishBase : DFish
{
    
    protected override void OnSpawnedFish()
    {
        FLManagement.RegisterFishBase(this);
    }

    public void MakeActiveFish(int index)
    {
        if (CheckFishIndex(index))
        {
            transformPool[index].gameObject.AddComponent<FLControllableFish>();
            //transformPool[index].gameObject.AddComponent<ControllableFish>();
            
        }
        
    }
    public void ReleaseActiveFish(int index)
    {
        if (CheckFishIndex(index))
        {
            FLControllableFish flc = transformPool[index].gameObject.GetComponent<FLControllableFish>();
            if (flc != null)
            {
                flc.ReleaseControl();
            }

        }
    }
}

