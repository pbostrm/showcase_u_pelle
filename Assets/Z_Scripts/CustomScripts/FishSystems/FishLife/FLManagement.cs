﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FLManagement : PMonoBehaviour
{


    #region static

    static FLManagement flm;

    static public void Init()
    {
        if (flm == null)
        {
            GameObject g = GameObject.Find("_flm");
            if (g == null)
            {
                g = new GameObject("_flm");
            }
            flm = g.GetComponent<FLManagement>();
            if (flm == null)
            {
                flm = g.AddComponent<FLManagement>();
            }
        }
    }
    static public void RegisterFishBase(FLFishBase flfBase)
    {
        Init();
        flm.RegFishBase(flfBase);
    }
    #endregion

    FLFishBase activeFishBase;
    int activeFishIndex = 0;
    List<FLFishBase> fishBases;


    public void RegFishBase(FLFishBase flfBase)
    {
        if (fishBases == null)
        {
            fishBases = new List<FLFishBase>();
        }
        if (!fishBases.Contains(flfBase))
        {
            fishBases.Add(flfBase);
        }

        if (activeFishBase == null)
        {
            setActiveFish(flfBase);
        }
    }
    void setActiveFish(FLFishBase flfBase)
    {
        activeFishBase = flfBase;
        activeFishIndex = 0;

        flfBase.MakeActiveFish(activeFishIndex);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            GetNextRandomFish();
        }
    }

    void GetNextRandomFish()
    {
        ReleaseActiveFish();
        activeFishBase = fishBases[Random.Range(0, fishBases.Count)];
        activeFishIndex = Random.Range(0, activeFishBase.totalFish);
        activeFishBase.MakeActiveFish(activeFishIndex);
    }
    void ReleaseActiveFish()
    {
        if (activeFishBase != null)
        {
            activeFishBase.ReleaseActiveFish(activeFishIndex);
            activeFishBase = null;
        }
    }

}