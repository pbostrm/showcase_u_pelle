﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

partial class GoldFishBase : DFish 
{
    static public  List<GoldFishBase> goldFishGroups;
    public List<int> warmedFishIndices;

    float[] warmth;
    bool[] warming;
    public bool[] isWarmed;
    public bool[] isFound;
    bool[] idle;
    bool[] selected;

    Vector3[] moveToTarget;
    float[] workSpeed;
    float[] targetSpeed;

    public byte[] minionTarget;

    public int selectedCnt;
    public bool selectedGroup;


    int idleCnt;

    public void Awake()
    {
        if (goldFishGroups == null)
        {
            goldFishGroups = new List<GoldFishBase>();
        }
        goldFishGroups.Add(this);
        if (warmedFishIndices == null)
        {
            warmedFishIndices = new List<int>();
        }

        base.Awake();


        updateBehaviours[1] = RoamingBehaviour;
        updateBehaviours[2] = FollowerBehaviour;
        updateBehaviours[3] = SelectedAndIdle;

        warmth = new float[totalFish];
        warming = new bool[totalFish];
        isWarmed = new bool[totalFish];
        isFound = new bool[totalFish];
        idle = new bool[totalFish];
        selected = new bool[totalFish];

        moveToTarget = new Vector3[totalFish];
        workSpeed = new float[totalFish];
        targetSpeed = new float[totalFish];
        minionTarget = new byte[totalFish];
        for (int i = 0; i < totalFish;i++ )
        {
            moveToTarget[i] = new Vector3();
            UpdateBehaviourID[i] = 1;
            workSpeed[i] = speed;
            targetSpeed[i] = speed;
            idle[i] = true;

        }
    }
    


    public override void SyncTree()
    {
        base.SyncTree();

        //int selCnt = selectedCnt;
        if (gotPresyncModules)
        {
            if (presyncModules != null)
            {
                for (int i = 0; i < presyncModules.Count; i++)
                {
                    presyncModules[i](this);
                }
            }
            presyncModules.Clear();
            gotPresyncModules = false;
        }
        for (int i = 0; i < totalFish; i++)
        {
                if (isWarmed[i])
                {
                    if (selectedGroup)
                    {
                        if (UpdateBehaviourID[i] == 2)
                        {
                            UpdateBehaviourID[i] = 3;

                        }
                        //selected[i] = true;
                    }
                    else
                    {
                        if (UpdateBehaviourID[i] == 3)
                        {
                            UpdateBehaviourID[i] = 2;

                        }
                    }
                }
                else
                {
                    UpdateBehaviourID[i] = 1;

                }
        }
    }
    public override void SyncPost()
    {
        for (int i = 0; i < totalFish; i++)
        {
            fishSystem.targetVectors[startIndex + i] = workTargetVectors[i];
            float speedDelta = ( targetSpeed[i] - workSpeed[i]);
           // workSpeed[i] = targetSpeed[i];
            //workSpeed[i] += Mathf.Min(speedDelta,speedDelta*deltaTime);
            workSpeed[i] = Mathf.Clamp(workSpeed[i] +(speedDelta*deltaTime), speed, speed * 10.0f);
            fishSystem.fishMovementSpeed[startIndex + i] = workSpeed[i];
            fishSystem.rotationalSpeed[startIndex + i] = workSpeed[i]*0.5f;
        }


        LightningMagnet lr;

        for (int i = 0; i < transformPool.Length; i++)
        {

            if (isWarmed[i] && !isFound[i])
            {
                if (SceneSystem.currentActiveSceneSystem != null)
                {
                    SceneSystem.currentActiveSceneSystem.WarmedFishCnt++;
                    isFound[i] = true;
                    warmedFishIndices.Add(i);
                    idle[i] = true;
                    idleCnt++;
                 
                }

            }
            graphicControls[i].targetDirection = fishSystem.fishVectors[startIndex + i];
            graphicControls[i].Warmth = warmth[i];
            graphicControls[i].speed = workSpeed[i];


            if (warming[i])
            {
                lr = transformPool[i].GetComponent<LightningMagnet>();
                if (lr == null)
                {
                    lr = transformPool[i].gameObject.AddComponent<LightningMagnet>();
                    lr.source = PlayerFishSystem.player.playerTransform;
                }
            }
            else
            {
                lr = transformPool[i].GetComponent<LightningMagnet>();
                if (lr != null)
                {
                    lr.QueueDestroy();
                    //Destroy(lr);
                }
            }
        }
        
        deltaTime = Time.time - lastTime;

        lastTime = Time.time;

        subSystem.StopTimer();
        subSystem.updatingFish = false;
    }

    public void FollowerBehaviour(int index)
    {
        delta = (PlayerFishSystem.playerPoint - localTree.points[index]);

        if (delta.sqrMagnitude > 250.0f)
        {
           // workSpeed[index] = speed*2.0f;

            MoveToTargetBehaviour(index);
        }
        else
        {
            targetSpeed[index] = speed;

            IdleAroundPlayer(index);
        }
    }
    public void MoveToTargetBehaviour(int index)
    {
        groupingVector = Vector3.zero;

        matchingVector = delta;

        proximityCounter = 0;

        indiceIndex = localTree.invertedIndices[index];
        if (indiceIndex < 10)
        {
            indiceIndex = 10;
        }
        workTargetVectors[index] = (workRepelVectors[index]) +
                                groupingVector +
                                (matchingVector * matchingVectorModifier);

    }
    public void IdleAroundPlayer(int index)
    {
        groupingVector = Vector3.zero;
        if (fishSystem.fishMotivations[index] != Vector3.zero)
        {
            matchingVector = fishSystem.fishMotivations[index];//fishMotivations[i]; //motivations plox
        }
        else
        {

            matchingVector = workVectors[index];
        }
        proximityCounter = 0;

        indiceIndex = localTree.invertedIndices[index];
        if (indiceIndex < 10)
        {
            indiceIndex = 10;
        }

        for (int j = indiceIndex - 10; (j < localTree.indices.Length) && (j < indiceIndex + 10); j++)
        {

            delta = (localTree.points[index] - localTree.points[localTree.indices[j]]);

            if (delta.sqrMagnitude < 130.0f)
            {
                //attractiveness
                proximityCounter++;
                if (attractiveness[localTree.indices[j]] > attractiveness[index])
                {
                    aModifier = 1.0f + attractiveness[localTree.indices[j]] - attractiveness[index];
                }
                else
                {
                    aModifier = 1.0f - attractiveness[index] - attractiveness[localTree.indices[j]];

                }
                groupingVector -= delta;//(localTree.points[i] + -delta);
                matchingVector += workVectors[localTree.indices[j]] * aModifier;
            }
        }
        groupingVector = (groupingVector / (proximityCounter));// -localTree.points[i];
        workTargetVectors[index] = (workRepelVectors[index]) +
                                groupingVector +
                                (matchingVector * matchingVectorModifier);
    }
  
    public void RoamingBehaviour(int index)
    {

        //groupingVector = localTree.points[index];
        groupingVector = Vector3.zero;
        if (fishSystem.fishMotivations[index] != Vector3.zero)
        {
            matchingVector = fishSystem.fishMotivations[index];//fishMotivations[index]; //motivations plox
        }
        else
        {

            matchingVector = workVectors[index];
        }
        proximityCounter = 0;
        //fishVectors[index] = Vector3.zero;

        indiceIndex = localTree.invertedIndices[index];
        if (indiceIndex < 10)
        {
            indiceIndex = 10;
        }
        //Warmth

        if (!isWarmed[index])
        {
            delta = (PlayerFishSystem.playerPoint - localTree.points[index]);
            if (delta.sqrMagnitude < 100.0f)
            {
                warmth[index] += deltaTime * 5.0f; // we are not on linear time, need to move this one somewher
                warming[index] = true;

            }
            else if (warmth[index] < 1.0f)
            {

                warmth[index] -= deltaTime * 2.0f;
                warming[index] = false;


            }
            warmth[index] = Mathf.Clamp(warmth[index], 0.0f, 2.0f);
            if (warmth[index] >= 2.0f)
            {
                isWarmed[index] = true;
                UpdateBehaviourID[index] = 2; //move up to follower behaviour;
                //delta = (localTree.points[index] - localTree.points[0]);


                warming[index] = false;
            }
        }
        if (isWarmed[index])
        {
            delta = (PlayerFishSystem.playerPoint - localTree.points[index]);
            if (delta.sqrMagnitude >= 100)
            {
                groupingVector += delta * 5;
                //matchingVector += workVectors[0];
                proximityCounter++;
            }
        }
        for (int j = indiceIndex - 10; (j < localTree.indices.Length) && (j < indiceIndex + 10); j++)
        {
            /*if(PlayableFish)
            {
                if (warmth[index] >= 3.0f)
                {
                    if (localTree.indices[j] == 0)
                    {
                        continue; 
                    }
                }   
            }*/
            if (!(isWarmed[index] == isWarmed[localTree.indices[j]]) && localTree.indices[j] != 0)
            {
                continue;
            }

            delta = (localTree.points[index] - localTree.points[localTree.indices[j]]);

            if (delta.sqrMagnitude < 130.0f)
            {

                //attractiveness
                proximityCounter++;
                if (attractiveness[localTree.indices[j]] > attractiveness[index])
                {
                    aModifier = 1.0f + attractiveness[localTree.indices[j]] - attractiveness[index];
                }
                else
                {
                    aModifier = 1.0f - attractiveness[index] - attractiveness[localTree.indices[j]];

                }
                groupingVector -= delta;//(localTree.points[index] + -delta);
                matchingVector += workVectors[localTree.indices[j]] * aModifier;
            }
        }
        groupingVector = (groupingVector / (proximityCounter));// -localTree.points[index];

        workTargetVectors[index] = (workRepelVectors[index]) +
                                groupingVector +
                                (matchingVector * matchingVectorModifier);
    }
    public void SelectedAndIdle(int index)
    {
        groupingVector = Vector3.zero;
        if (fishSystem.fishMotivations[index] != Vector3.zero)
        {
            matchingVector = fishSystem.fishMotivations[index];//fishMotivations[i]; //motivations plox
        }
        else
        {

            matchingVector = workVectors[index];
        }


        delta = (PlayerFishSystem.playerPoint - localTree.points[index]);

        if (delta.sqrMagnitude > 50.0f)
        {
            targetSpeed[index] = speed * 5.0f;
        }
        else
        {
            targetSpeed[index] = speed;

        }
        groupingVector = delta.normalized;
        matchingVector += PlayerFishSystem.playerDir;



        workTargetVectors[index] = (workRepelVectors[index]) +
                                groupingVector +
                                (matchingVector * 0.5f);
    }
}