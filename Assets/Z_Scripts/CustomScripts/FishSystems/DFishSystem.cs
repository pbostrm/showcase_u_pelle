﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using System.Threading;

public class DFishSystem : MonoBehaviour {

    static List<DFishSystem> fishSystems;

    public int totalFish = 0;
    public int FishInObstacle = 0;

    float boundaryRadius = 100; //Del

    //public int activeFish = 256;


    //public float[] attractiveness;
    public float[] fishMovementSpeed;

    public Vector3[] fishPoints;
    public Vector3[] fishVectors;
    public Vector3[] targetVectors;
    public Vector3[] fishPriorityVectors;
    public Vector3[] fishMotivations; 
    public Vector3[] repelVectors;
    public float[] rotationalSpeed;

    //Work data
    public Vector3[] workRepelVectors;
    public Vector3[] workPriorityVectors;
    public Vector3[] workPoints;
    public Vector3[] workTargetVectors;

	//Transform[] fishTransforms; 



    public VPTree mainTree;
    public DOctree doctree;

    Thread UpdateSchoolThread;


    public bool enableDOctree;
    public string OctreeName;
    public string spawnName; //del



    public bool PlayableFish; //del
    public delegate void SpawnFish();
    //public delegate void PrepareJob();
    List<SubFishSystem> subSystems;

    public class SubFishSystem
    {
        public SubFishSystem(int sI, int c, SpawnFish sF, TaskPool.Task ufJ, TaskPool.Task pJ, TaskPool.Task poJ)
        {
            startIndex = sI;
            count = c;
            spawnFish = sF;
            updateFishJob = ufJ;
            prepareJob = pJ;
            postJob = poJ;
            rwLock = new ReaderWriterLockSlim();
        }
        public string subSystemName;
        public int startIndex;
        public int count;
        public SpawnFish spawnFish;
        public TaskPool.Task updateFishJob;
        public TaskPool.Task prepareJob;
        public TaskPool.Task postJob;
        public bool updatingFish;
        public ReaderWriterLockSlim rwLock;
        public float LastUpdateTime;
        float startTime;

        public void StartTimer()
        {
            startTime = Time.time;
        }

        public void StopTimer()
        {
            LastUpdateTime = Time.time - startTime;
        }
    }
    public void Awake()
    {
        //subSystems = new List<SubFishSystem>();
    }
    static public DFishSystem GetFishSystem(string OctreeName)
    {
        if (fishSystems == null)
        {
            fishSystems = new List<DFishSystem>();
        }

        DFishSystem targetSystem = null;
        foreach (var sys in fishSystems)
        {
            if (sys.OctreeName == OctreeName)
            {
                targetSystem = sys;
            }
        }

        if (targetSystem == null)
        {
            GameObject g = new GameObject("_DFishSystem_" + OctreeName);

            targetSystem = g.AddComponent<DFishSystem>();
            targetSystem.OctreeName = OctreeName;
        }
        fishSystems.Add(targetSystem);

        return targetSystem;
    }
    public void OnDestroy()
    {
        if (fishSystems != null)
        {
            fishSystems.Remove(this);
        }
    }
    public int subsystemCnt;
    public SubFishSystem InjectFish(int count, SpawnFish spawnFish, TaskPool.Task ufJ, TaskPool.Task pJ, TaskPool.Task postjob)
    {
        
        int startindex = totalFish;

        totalFish = totalFish + count;

        SubFishSystem sfs = new SubFishSystem(startindex, count, spawnFish,ufJ,pJ,postjob);
        if (subSystems == null)
        {
            subSystems = new List<SubFishSystem>();
        }
        subSystems.Add(sfs);
        subsystemCnt++;
        if (OctreeName != null || OctreeName != "")
        {
            enableDOctree = true;
        }
        return sfs;
    }

	void Start () 
	{
       
        if (enableDOctree)
        {
            doctree = new DOctree(OctreeName,transform.position);
            if (doctree != null)
            {
                boundaryRadius = doctree.size;
            }
        }


        fishPoints = new Vector3[totalFish];
        fishVectors = new Vector3[totalFish];
        targetVectors = new Vector3[totalFish];

        fishPriorityVectors = new Vector3[totalFish];
        fishMotivations = new Vector3[totalFish];

        fishMovementSpeed = new float[totalFish];
        rotationalSpeed = new float[totalFish];
        repelVectors = new Vector3[totalFish];

        //Work data
        workRepelVectors = new Vector3[totalFish];
        workPoints = new Vector3[totalFish];
        workPriorityVectors = new Vector3[totalFish];
        workTargetVectors = new Vector3[totalFish];

        //
        if (subSystems != null)
        {
            for (int i = 0;i < subSystems.Count; i++)
            {
                subSystems[i].spawnFish();
            }
            
        }

        mainTree = new VPTree(totalFish);
        //mainTree.points = fishPoints;



    }
  
    float UpdateThreadTimer;
    float LastThreadUpdate;
    bool schoolUpdateWorking = false;
    public bool hasUpdatedSystems = true;
	void Update ()
	{


        UpdateThreadTimer += Time.deltaTime;

        if (!schoolUpdateWorking )
        {

            if(pollSubSystems())
            {
                //schoolUpdateWorking = true;


                SyncWorkData();
                //UpdateRepel();
                //SyncPostData();
                TaskPool.QueueTask(UpdateRepel,SyncPostData);
                //UpdateRepel();
                //TaskPool.QueueTask(UpdateSchool);

                LastThreadUpdate = UpdateThreadTimer;
                UpdateThreadTimer = 0.0f;

                hasUpdatedSystems = false;
            }
        }
        NormalizeVectors();

        MoveFish();

	}
    
    bool pollSubSystems()
    {
        for (int i = 0; i < subSystems.Count; i++)
        {
            subSystems[i].rwLock.EnterUpgradeableReadLock();
            try
            {
                if (subSystems[i].updatingFish)
                {
                    return false;
                }

            }
            finally
            {
                subSystems[i].rwLock.ExitUpgradeableReadLock();

            }
        }
        return true;
    }
    public void SyncWorkData()
    {
        schoolUpdateWorking = true;
        //DebugOutput.Shout("prutt");
        mainTree.PushTree(fishPoints);
        NormalizeVectors();
        for (int i = 0; i < totalFish;i++ )
        {
            workRepelVectors[i] = repelVectors[i];
            workPoints[i] = fishPoints[i];
            workTargetVectors[i] = targetVectors[i];
            workPriorityVectors[i] = fishPriorityVectors[i];
        }
    }
    public void SyncPostData()
    {
        //DebugOutput.Shout("prutt2");

        for (int i = 0; i < totalFish; i++)
        {
            repelVectors[i] = workRepelVectors[i];
            //fishPoints[i] = workPoints[i];
            //targetVectors[i] = workTargetVectors[i];
            fishPriorityVectors[i] = workPriorityVectors[i];
        }

        for (int i = 0; i < subSystems.Count;i++ )
        {
            subSystems[i].prepareJob();
        }
        for (int i = 0; i < subSystems.Count; i++)
        {
            TaskPool.QueueTask(subSystems[i].updateFishJob, subSystems[i].postJob);
        }
        schoolUpdateWorking = false;

    }
    

    void UpdateRepel()
    {
        //NormalizeVectors(); //might not want to do this.
        //DebugOutput.Shout("prutt3");

        UpdateAvoidance();
        mainTree.HeapFullTest();

        int indiceIndex = 0;

        Vector3 repelVector = new Vector3();

        Vector3 delta = new Vector3();

        int proximityCounter = 0;

        for (int i = 0; i < workPoints.Length; i++)
        {
            if (workPriorityVectors[i] != Vector3.zero)
            {
                //   continue; 
                workTargetVectors[i] = workPriorityVectors[i];
                continue;
            }

            repelVector = Vector3.zero;

            proximityCounter = 0;


            indiceIndex = mainTree.invertedIndices[i];
            if (indiceIndex < 10)
            {
                indiceIndex = 10;
            }

            for (int j = indiceIndex - 10; (j < mainTree.indices.Length) && (j < indiceIndex + 10); j++)
            {
                delta = (workPoints[i] - workPoints[mainTree.indices[j]]);

                if (delta.sqrMagnitude < 50.0f)
                {
                    proximityCounter++;
                    if (delta.sqrMagnitude < 5)
                    {
                        repelVector = repelVector + delta.normalized;

                    }
                }
            }

            if (proximityCounter > 0)
            {

                if (float.IsNaN(repelVector.x))
                {
                    repelVector = Vector3.zero;
                }
                workRepelVectors[i] = repelVector;
                /*targetVectors[i] = (repelVector) +
                                groupingVector +
                                (matchingVector * 0.5f);*/
            }
        }
    }
   /* void UpdateSchool()
    {
       // UnityEngine.Debug.Log("banan");

        schoolUpdateWorking = true;
        //NormalizeVectors(); //might not want to do this.

        UpdateAvoidance();
        mainTree.HeapFullTest();
        int indiceIndex = 0;

        Vector3 repelVector = new Vector3();
        Vector3 groupingVector = new Vector3();
        Vector3 matchingVector = new Vector3();
        Vector3 delta = new Vector3();

        int proximityCounter = 0;
        float aModifier = 0.0f;

        for (int i = 0; i < fishPoints.Length; i++)
        {
            if (fishPriorityVectors[i] != Vector3.zero)
            {
             //   continue; 
                targetVectors[i] = fishPriorityVectors[i];
                continue;
            }

            repelVector = Vector3.zero;

            groupingVector = fishPoints[i];
            if (fishMotivations[i] != Vector3.zero)
            {
                matchingVector = fishMotivations[i];//fishMotivations[i]; //motivations plox
            }
            else
            {
                matchingVector = fishVectors[i];
            }
            proximityCounter = 0;
            //fishVectors[i] = Vector3.zero;

            indiceIndex = mainTree.invertedIndices[i];
            if (indiceIndex < 10 )
            {
                indiceIndex = 10;
            }

            for (int j = indiceIndex - 10; (j < mainTree.indices.Length) && (j < indiceIndex + 10); j++)
            {
                delta = (fishPoints[i] - fishPoints[mainTree.indices[j]]);

                if (delta.sqrMagnitude < 50.0f)
                {
                    proximityCounter++;
                    if (delta.sqrMagnitude < 5)
                    {
                        repelVector = repelVector + delta.normalized;

                    }
                    if (attractiveness[mainTree.indices[j]] > attractiveness[i])
                    {
                        aModifier = 1.0f + attractiveness[mainTree.indices[j]] - attractiveness[i];
                    }
                    else
                    {
                        aModifier = 1.0f - attractiveness[i] - attractiveness[mainTree.indices[j]];

                    }
                    groupingVector += (fishPoints[i] + -delta);
                    matchingVector += fishVectors[mainTree.indices[j]] * aModifier;
                }
            }
            groupingVector =  (groupingVector / (proximityCounter + 1)) - fishPoints[i];

            if (proximityCounter > 0)
            {
                if (float.IsNaN(repelVector.x))
                {
                    repelVector = Vector3.zero;
                }
                targetVectors[i] = (repelVector) +
                                groupingVector +
                                (matchingVector*0.5f);
            }
        }
        schoolUpdateWorking = false;
        
    }
    * */
    void UpdateAvoidance()
    {

        if (doctree != null)
        {
            FishInObstacle = 0;
            int obstacleNode = 0;
            for (int i = 0; i < totalFish; i++)
            {
                workPriorityVectors[i] = Vector3.zero;

                //boundaries
                if (workPoints[i].y > doctree.position.y + boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.down;

                }
                else if (workPoints[i].y < doctree.position.y - boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.up;

                }
                if (workPoints[i].x > doctree.position.x + boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.left;

                }
                else if (workPoints[i].x < doctree.position.x - boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.right;

                }

                if (workPoints[i].z > doctree.position.z + boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.back;

                }
                else if (workPoints[i].z < doctree.position.z - boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.forward;

                }

                //octree

                obstacleNode = doctree.checkObstacle(workPoints[i]);
                    if (obstacleNode != -1)
                    {
                        //fishPriorityVectors = 
                        doctree.findClosestEscape(obstacleNode, ref workPriorityVectors[i]);
                        FishInObstacle++;
                    }
                

            }
        }
        else
        {
            for (int i = 0; i < totalFish; i++)
            {
                workPriorityVectors[i] = Vector3.zero;

                //boundaries
                if (workPoints[i].y > 0)
                {
                    workPriorityVectors[i] += Vector3.down;
                }
                else if (workPoints[i].y < -(boundaryRadius * 2))
                {
                    workPriorityVectors[i] += Vector3.up;

                }
                if (workPoints[i].x > boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.left;

                }
                else if (workPoints[i].x < -boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.right;

                }

                if (workPoints[i].z > boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.back;

                }
                else if (workPoints[i].z < -boundaryRadius)
                {
                    workPriorityVectors[i] += Vector3.forward;

                }
            }
        }

        
    }
    void NormalizeVectors()
    {
        for (int i = 0; i < totalFish; i++)
        {
            fishVectors[i].Normalize();
        }
    }
	void MoveFish()
	{
        Vector3 TDnorm = new Vector3();
        Vector3 newRot = new Vector3(); 

		for (int i = 0; i < totalFish; i++)
		{
            if (fishPriorityVectors[i] != Vector3.zero)
            {
                //   continue; 
                targetVectors[i] = fishPriorityVectors[i];
            }
           // TDnorm = targetVectors[i].normalized;
            //targetVectors[i] = fishVectors[i];

            fishVectors[i] = Vector3.RotateTowards(fishVectors[i], targetVectors[i].normalized, rotationalSpeed[i] * Time.deltaTime, 1.0f);
            //fishVectors[i] = targetVectors[i].normalized;
            /* newRot.x = Vector3.RotateTowards(fishVectors[i], TDnorm, 1.0f * Time.deltaTime, 1.0f).x;
            newRot.y = Vector3.RotateTowards(fishVectors[i], TDnorm, 1.0f * Time.deltaTime, 1.0f).y;
            newRot.z = Vector3.RotateTowards(fishVectors[i], TDnorm, 1.0f * Time.deltaTime, 1.0f).z;
            */
            //possible to just lerp floats
            //fishVectors[i] = Vector3.RotateTowards;

            //fishVectors[i] = newRot;
            if (float.IsNaN(fishVectors[i].x))
            {
                fishVectors[i] = targetVectors[i];
            }
            //fishVectors[i] = targetVectors[i];
            fishPoints[i] = fishPoints[i] + (fishVectors[i].normalized * Time.deltaTime * fishMovementSpeed[i]);
		}
	}




    public void OnGUI()
    {
        //GUI.Label(new Rect(30, 200, 250, 100), "Time updating: " + (LastThreadUpdate * 1000).ToString() + " ms.");
        if (subSystems != null)
        {
            for (int i = 0; i < subSystems.Count; i++)
            {
                string s = subSystems[i].subSystemName + " UpdateTime: " + (subSystems[i].LastUpdateTime*1000.0f).ToString("000.00")+" ms";
                GUI.Label(new Rect(Screen.width - s.Length * 6.0f, 40 +(i*20.0f), s.Length * 6.0f, 25f), s);

            }
        }
    }
}
