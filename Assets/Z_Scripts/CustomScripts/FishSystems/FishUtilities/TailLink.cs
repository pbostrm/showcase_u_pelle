﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class TailLink : MonoBehaviour
{
    public Transform target;
    public float distance;
    Vector3 dir;
    void Awake()
    {
        GameObject g = GameObject.Find("_TailLinks");
        if (g == null)
        {
            g = new GameObject("_TailLinks");
        }
        transform.parent = g.transform;
        dir = new Vector3();
    }
    void Update()
    {
        if (target != null)
        {
            dir = (target.position - transform.position);

            transform.forward = dir.normalized;

            dir = dir.normalized * distance;
            transform.position = target.position - dir;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}