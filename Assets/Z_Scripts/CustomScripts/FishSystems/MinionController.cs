﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class MinionController : MonoBehaviour
{
    static public MinionController minionController;
    static public bool isFocusing
    {
        get
        {
            if (minionController.activeTarget != null)
            {
                return true;
            }
            return false;
        }
    }
    int currentMinionBase = -1;

    Rect guiRect;

    List<GoldFishBase> goldfishGroups;

    int selectedMinionCnt;

    bool allMinionsAssembled;

    int targetLayer = 10;

    void Awake()
    {
        minionController = this;
        guiRect = new Rect(40.0f, Screen.height * 0.4f, 200, 30);

    }
    public void Update()
    {

        //Assemble all minions
        if (Input.GetKeyDown(KeyCode.A)) 
        {
            AssembleMinions();
        }
        //Suck in minion // count minion
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (checkGoldFishGroups())
            {
                SuckMinions();
            }
        }
        //Cycle out of minions
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (checkGoldFishGroups())
            {
                currentMinionBase = -1;

                for (int i = 0; i < goldfishGroups.Count; i++)
                {
                    goldfishGroups[i].selectedGroup = false;
                }


            }
        }
        //cycle available Minions;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (checkGoldFishGroups())
            {
                currentMinionBase++;
                if (currentMinionBase == goldfishGroups.Count)
                {
                    currentMinionBase = 0;
                }
                for (int i = 0; i < goldfishGroups.Count; i++)
                {
                    goldfishGroups[i].selectedGroup = false;
                }
                goldfishGroups[currentMinionBase].selectedGroup = true;

            }
        }

        if (currentMinionBase >= 0)
        {
            ScanForMinionTarget();
        }
    }
    MinionTargetComponent lastTarget;
    MinionTargetComponent activeTarget;
    void ScanForMinionTarget()
    {
        RaycastHit rayHit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out rayHit, 1000.0f, (1 << targetLayer)))
        {
            MinionTargetComponent mt = rayHit.collider.GetComponent<MinionTargetComponent>();
            if (mt != null)
            {
                activeTarget = mt;

                mt.isFocused = true;
                if (lastTarget == null)
                {
                    lastTarget = mt;
                }
                else if (lastTarget != mt)
                {
                    lastTarget.isFocused = false;
                    lastTarget = mt;
                }
            }
            
            Debug.DrawRay(ray.origin, ray.direction, Color.green);
        }
        else
        {
            activeTarget = null;
            if (lastTarget != null)
            {
                lastTarget.isFocused = false;
            }
            Debug.DrawRay(ray.origin, ray.direction, Color.red);
        }

    }
    void AssembleMinions()
    {
        if (allMinionsAssembled)
        {
            //release
            foreach (var group in goldfishGroups)
            {
                group.selectedCnt = 0;
            }
        }
        else
        {
            foreach (var group in goldfishGroups)
            {
                group.selectedCnt = group.activeFish;
            }
            //assemble;

        }
        allMinionsAssembled = !allMinionsAssembled;
        currentMinionBase = -1;
    }
    void SuckMinions()
    {
        if (currentMinionBase == -1)
        {
            if (goldfishGroups.Count >= 0)
            {
                currentMinionBase = 0;
            }
        }
        
        if (currentMinionBase >= 0)
        {
            //if(lastTarget)
            if (activeTarget != null)
            {
                activeTarget.AddMinion(1, goldfishGroups[currentMinionBase]);
            }
            goldfishGroups[currentMinionBase].selectedCnt++;
        }
    }
    bool checkGoldFishGroups()
    {
        if (goldfishGroups == null)
        {
            if (GoldFishBase.goldFishGroups != null)
            {
                goldfishGroups = GoldFishBase.goldFishGroups;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }

    }
    void OnGUI()
    {

        GUI.Label(guiRect, "Current goldfishgroup cycled: " + currentMinionBase.ToString());

        if (checkGoldFishGroups())
        {
            for (int i = 0; i < goldfishGroups.Count; i++)
            {
                GUI.Label(new Rect(40.0f, (Screen.height * 0.4f) + ((i+1) * 40.0f), 200, 30), goldfishGroups[i].FishName + " selected: " + goldfishGroups[i].selectedCnt.ToString());

            }
        }
        

    }
}
