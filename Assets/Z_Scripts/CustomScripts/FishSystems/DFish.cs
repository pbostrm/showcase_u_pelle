﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Threading;
using System.Diagnostics;

class DFish : MonoBehaviour
{
    public string OctreeName;
    public string SpawnName;
    public string FishName = "Fishie";

    //public bool PlayableFish;

    public int totalFish = 1;
    public int activeFish = 1;

    public float speed = 1.0f;

    public int startIndex;

    protected DFishSystem fishSystem;

    protected Transform[] transformPool;
    protected GraphicControl[] graphicControls;


    float boundaryRadius = 100; //Del

    public VPTree localTree;
    public bool UpdatingSchool = false;

    public DFishSystem.SubFishSystem subSystem;

    public float[] attractiveness;

    public Vector3[] workRepelVectors;
    public Vector3[] workPriorityVectors;
    public Vector3[] workVectors;
    public Vector3[] workTargetVectors;
    public byte[] UpdateBehaviourID;
   
    public float matchingVectorModifier = 0.6f;

    protected float lastTime;
    protected float deltaTime;

#region Workingvars
    protected Vector3 groupingVector;
    protected Vector3 matchingVector;
    protected Vector3 delta;
    protected int indiceIndex = 0;
    protected int proximityCounter = 0;
    protected float aModifier = 0.0f;
#endregion



    public delegate void UpdateBehaviour(int index);

    public UpdateBehaviour[] updateBehaviours;

    public void Awake()
    {
        if (updateBehaviours == null)
        {
            updateBehaviours = new UpdateBehaviour[25];
        }

        updateBehaviours[0] = DefaultBehaviour;

        activeFish = Mathf.Min(totalFish, activeFish);
        //Tell DFishSystem to allocate
        fishSystem = DFishSystem.GetFishSystem(OctreeName);
        subSystem = fishSystem.InjectFish(totalFish, SpawnFish,UpdateSchool,SyncTree,SyncPost);
        subSystem.subSystemName = gameObject.name;
        startIndex = subSystem.startIndex;
        transformPool = new Transform[activeFish];
        graphicControls = new GraphicControl[activeFish];
        localTree = new VPTree(totalFish);
        attractiveness = new float[totalFish];




        workRepelVectors = new Vector3[totalFish];
        workVectors = new Vector3[totalFish];
        workPriorityVectors = new Vector3[totalFish];
        workTargetVectors = new Vector3[totalFish];

        UpdateBehaviourID = new byte[totalFish];

        OnAwake();
        lastTime = Time.time;


    }
    protected virtual void OnAwake()
    {

    }
    public void Update()
    {
        PushTransforms();
       
    }

    public virtual void SyncTree()
    {
        subSystem.StartTimer();
        subSystem.updatingFish = true;
        //UnityEngine.Debug.Log("Banan");

        for (int i = 0; i < totalFish; i++)
        {
            localTree.points[i] = fishSystem.fishPoints[startIndex+i];

            workRepelVectors[i] = fishSystem.repelVectors[startIndex + i];
            workVectors[i] = fishSystem.fishVectors[i];
            workTargetVectors[i] = fishSystem.targetVectors[startIndex + i];
            workPriorityVectors[i] = fishSystem.fishPriorityVectors[startIndex + i];
        }
        localTree.SyncTree();
        lastTime = Time.time;
        
    }
    public virtual void SyncPost()
    {

        for (int i = 0; i < totalFish; i++)
        {
            //fishSystem.repelVectors[startIndex + i] = workRepelVectors[i];
            //fishPoints[i] = workPoints[i];
            fishSystem.targetVectors[startIndex + i] = workTargetVectors[i];

            //fishSystem.fishPriorityVectors[startIndex + i] = workPriorityVectors[i];
        }

       /* if (PlayableFish)
        {
            
            for (int i = 0; i < transformPool.Length; i++)
            {


                //graphicControls[i].targetDirection = workTargetVectors[i];
                graphicControls[i].targetDirection = fishSystem.fishVectors[startIndex + i];
                graphicControls[i].speed = fishSystem.fishMovementSpeed[startIndex + i];


            }
        }*/
        deltaTime = Time.time - lastTime;
        lastTime = Time.time;
        subSystem.StopTimer();
        subSystem.updatingFish = false;

    }
    public virtual void UpdateSchool()
    {
       // Stopwatch watch = new Stopwatch();
       // watch.Start();
       // SyncTree();
        localTree.HeapFullTest();



        for (int i = 0; i < totalFish; i++)
        {
            if (workPriorityVectors[i] != Vector3.zero)
            {
                //   continue; 
                workTargetVectors[i] = workPriorityVectors[i];
                continue;
            }

            updateBehaviours[UpdateBehaviourID[i]](i);

            
            
        }

    }

    protected virtual void PushTransforms()
    {

        Vector3 delta;
        for (int i = 0; i < transformPool.Length; i++)
        {
            delta = transformPool[i].position;
            transformPool[i].position = fishSystem.fishPoints[startIndex + i];
            delta = transformPool[i].position - delta;
            if (delta != Vector3.zero)
            {
                transformPool[i].forward = delta.normalized;
                transformPool[i].LookAt(fishSystem.fishPoints[startIndex+i]+delta, Vector3.up);

            }
            //graphicControls[i].targetDirection = fishSystem.targetVectors[startIndex + i];
        }

    }

    public void setSpeed(int index, float f)
    {
        if (index >= 0)
        {
            fishSystem.fishMovementSpeed[index] = f;
        }
    }
    public float getSpeed(int index)
    {
        if (index >= 0)
        {
            return fishSystem.fishMovementSpeed[index];
        }
        return 0.0f;
    }
    public void InputMotivation(int index, Vector3 v)
    {
        if (index >= 0 )
        {
            //fishVectors[index] = v;
            if (fishSystem.fishPriorityVectors[index] == Vector3.zero)
            {
                fishSystem.fishVectors[index] = Vector3.RotateTowards(fishSystem.fishVectors[index], v, 5.0f * Time.deltaTime, 1.0f);

            }

        }
    }
    public void InputAttractiveness(int index, float v)
    {
        if (index >= 0 && index < totalFish)
        {
            //  attractiveness[index] = v;
        }
    }

    public virtual void SpawnFish()
    {
        int randomSchoolSize = -1;

        Vector3 RandomPos = new Vector3();

        if (SpawnName != null && SpawnName != "" && fishSystem.doctree != null)
        {
            DQueryArea spawnArea = DQueryArea.LoadAreaFromFile("spawn/" + SpawnName);
            if (spawnArea != null)
            {
                for (int i = startIndex; i < startIndex+activeFish; i++)
                {
                    fishSystem.fishPoints[i] = fishSystem.doctree.GetWorldCoord(spawnArea.getRandomNode());

                    fishSystem.fishVectors[i] = Random.rotation.eulerAngles.normalized * 2 - Vector3.one;


                    fishSystem.targetVectors[i] = fishSystem.fishVectors[i];

                    attractiveness[i-startIndex] = Random.Range(0.5f, 1.0f);
                    fishSystem.fishMovementSpeed[i] = speed;
                    fishSystem.rotationalSpeed[i] = 0.9f;
                }
            }
        }
        else
        {
            for (int i = startIndex; i < startIndex + activeFish; i++)
            {
                if (randomSchoolSize < 0)
                {
                    randomSchoolSize = Random.Range(0, 1);
                    RandomPos.x = Random.Range(-boundaryRadius, boundaryRadius);
                    RandomPos.y = Random.Range(-(2.0f * boundaryRadius), 0.0f);
                    RandomPos.z = Random.Range(-boundaryRadius, boundaryRadius);
                }
                randomSchoolSize--;

                fishSystem.fishPoints[i] = RandomPos;
                fishSystem.fishPoints[i].x += Random.Range(-5.0f, 5.0f);
                fishSystem.fishPoints[i].y += Random.Range(-5.0f, 5.0f);
                fishSystem.fishPoints[i].z += Random.Range(-5.0f, 5.0f);

                fishSystem.fishPoints[i] = transform.position + fishSystem.fishPoints[i];

                fishSystem.fishVectors[i] = Random.rotation.eulerAngles.normalized * 2 - Vector3.one;

                // fishMotivations[i] = fishVectors[i];
                fishSystem.targetVectors[i] = fishSystem.fishVectors[i];

                attractiveness[i] = Random.Range(0.5f, 1.0f);
                fishSystem.fishMovementSpeed[i] = speed;
            }
        }



        Transform t = transform.GetChild(0);

        for (int i = 0; i < activeFish; i++)
        {
           /* if (PlayableFish&&i==0)
            {
                GameObject g = new GameObject();
                transformPool[i] =g.transform;
                //transformPool[i].name = "MasterGoblin";

            }
            else
            {
                transformPool[i] = ((GameObject)Instantiate(t.gameObject)).transform;
                transformPool[i].name = FishName + "_" + i.ToString();
                

                //transformPool[i].renderer.sharedMaterial = new Material(transformPool[i].renderer.material);
                //transformPool[i].renderer.sharedMaterial.color = UtilityColor.hsv2rgb(attractiveness[i] * 0.66f, 1.0f, 1.0f);
            
            
            }*/
            transformPool[i] = ((GameObject)Instantiate(t.gameObject)).transform;
            transformPool[i].name = FishName + "_" + i.ToString();

            graphicControls[i] = transformPool[i].gameObject.AddComponent<GraphicControl>();
            transformPool[i].parent = transform;

            

        }
        OnSpawnedFish();
    }
    protected virtual void OnSpawnedFish()
    {

    }
    public void DefaultBehaviour(int index)
    {

        //groupingVector = localTree.points[i];
        groupingVector = Vector3.zero;
        if (fishSystem.fishMotivations[index] != Vector3.zero)
        {
            matchingVector = fishSystem.fishMotivations[index];//fishMotivations[i]; //motivations plox
        }
        else
        {

            matchingVector = workVectors[index];
        }
        proximityCounter = 0;
        //fishVectors[i] = Vector3.zero;

        indiceIndex = localTree.invertedIndices[index];
        if (indiceIndex < 10)
        {
            indiceIndex = 10;
        }


        for (int j = indiceIndex - 10; (j < localTree.indices.Length) && (j < indiceIndex + 10); j++)
        {


            delta = (localTree.points[index] - localTree.points[localTree.indices[j]]);

            if (delta.sqrMagnitude < 130.0f)
            {

                //attractiveness
                proximityCounter++;
                if (attractiveness[localTree.indices[j]] > attractiveness[index])
                {
                    aModifier = 1.0f + attractiveness[localTree.indices[j]] - attractiveness[index];
                }
                else
                {
                    aModifier = 1.0f - attractiveness[index] - attractiveness[localTree.indices[j]];

                }
                groupingVector -= delta;//(localTree.points[i] + -delta);
                matchingVector += workVectors[localTree.indices[j]] * aModifier;
            }
        }
        groupingVector = (groupingVector / (proximityCounter));// -localTree.points[i];

        workTargetVectors[index] = (workRepelVectors[index]) +
                                groupingVector +
                                (matchingVector * matchingVectorModifier);
    }
    void OnDrawGizmos()
    {/*
        for (int i = 0; i < totalFish;i++ )
        {
            if (UpdateBehaviourID != null && UpdateBehaviourID.Count != 0)
            {
                if (UpdateBehaviourID[i] == 0)
                {
                    Gizmos.color = Color.grey;
                    Gizmos.DrawWireSphere(fishSystem.fishPoints[startIndex + i], 1.0f);
                }
                else if (UpdateBehaviourID[i] == 1)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawWireSphere(fishSystem.fishPoints[startIndex + i], 1.0f);
                }
                else if (UpdateBehaviourID[i] == 2)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireSphere(fishSystem.fishPoints[startIndex + i], 1.0f);
                }
                else if (UpdateBehaviourID[i] == 3)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireSphere(fishSystem.fishPoints[startIndex + i], 1.0f);
                }
                else if (UpdateBehaviourID[i] == 4)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawWireSphere(fishSystem.fishPoints[startIndex + i], 1.0f);
                }
            }
            
                
        }*/
    }
    protected bool CheckFishIndex(int index)
    {
        return index < totalFish;

    }

}
