﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

class PlanktonSystem : DFish
{
    protected ParticleSystem.Particle[] particles;

    float preHeatSpeed;
    public float goalAlpha;
    float[] workSpeed;

    float preheatTimer = 8.0f;
    float fadeInTimer = 5.0f;

    public bool randomColor;

    public override void SpawnFish()
    {
        preHeatSpeed = 10.0f;
        if (SpawnName != null && SpawnName != "" && fishSystem.doctree != null)
        {
            DQueryArea spawnArea = DQueryArea.LoadAreaFromFile("spawn/" + SpawnName);
            if (spawnArea != null)
            {
                for (int i = startIndex; i < startIndex + activeFish; i++)
                {
                    fishSystem.fishPoints[i] = fishSystem.doctree.GetWorldCoord(spawnArea.getRandomNode());

                    fishSystem.fishVectors[i] = Random.rotation.eulerAngles.normalized * 2 - Vector3.one;


                    fishSystem.targetVectors[i] = fishSystem.fishVectors[i];

                    attractiveness[i - startIndex] = Random.Range(0.5f, 1.0f);
                    fishSystem.fishMovementSpeed[i] = speed;
                    fishSystem.rotationalSpeed[i] = 0.9f;

                }
            }
        }
        workSpeed = new float[totalFish];
        particles = new ParticleSystem.Particle[totalFish];
        for (int i = 0; i < totalFish; i++)
        {
            particles[i].position = fishSystem.fishPoints[startIndex + i];
            particles[i].size = Random.Range(0.1f, 5.0f);
            particles[i].rotation = Random.Range(0.0f, 360.0f);
            //particles
            if (randomColor)
            {
                particles[i].color = UtilityColor.hsv2rgb((attractiveness[i]) * 0.5f, 1.0f, 1.0f);

            }
            else
            {
                particles[i].color = Color.white;
            }
            workSpeed[i] = speed;

        }
        
        particleSystem.maxParticles = totalFish;
        StartCoroutine(PreHeat());

        updateBehaviours[1] = StandardBehaviour;
    }
    public IEnumerator PreHeat()
    {
        while (preheatTimer > 0.0f)
        {
            preheatTimer -= Time.deltaTime;

            Color col;
            for (int i = 0; i < totalFish; i++)
            {
                fishSystem.fishMovementSpeed[i] = preHeatSpeed;
                col = particles[i].color;
                col.a = 0.0f;
                particles[i].color = col;
            }
            yield return 0;
        }
        while (fadeInTimer > 0.0f)
        {
            fadeInTimer -= Time.deltaTime;

            Color col;
            for (int i = 0; i < totalFish; i++)
            {
                fishSystem.fishMovementSpeed[i] = speed;

                col = particles[i].color;
                col.a = Mathf.Lerp(0.0f, goalAlpha, Mathf.Pow(1.0f - (fadeInTimer / 5.0f), 3));
                particles[i].color = col;
            }
            yield return 0;
        }
        for (int i = 0; i < totalFish; i++)
        {
            UpdateBehaviourID[i] = 1;

        }

    }

    public override void SyncPost()
    {
        for (int i = 0; i < totalFish; i++)
        {

            fishSystem.targetVectors[startIndex + i] = workTargetVectors[i];
            fishSystem.fishMovementSpeed[startIndex + i] = workSpeed[i];
            fishSystem.rotationalSpeed[startIndex + i] = workSpeed[i];
        }

        subSystem.StopTimer();
        subSystem.updatingFish = false;
    }
    float VortexDistance = 10.0f;
    public void StandardBehaviour(int index)
    {
        groupingVector = Vector3.zero;
        if (fishSystem.fishMotivations[index] != Vector3.zero)
        {
            matchingVector = fishSystem.fishMotivations[index];//fishMotivations[i]; //motivations plox
        }
        else
        {

            matchingVector = workVectors[index];
        }
        proximityCounter = 0;


        indiceIndex = localTree.invertedIndices[index];
        if (indiceIndex < 10)
        {
            indiceIndex = 10;
        }

        delta = (localTree.points[index]-PlayerFishSystem.playerPoint);
        if (delta.sqrMagnitude < VortexDistance*VortexDistance)
        {
            float dot = Vector3.Dot(delta.normalized, PlayerFishSystem.playerDir.normalized);
            float distanceRatio = 1.0f - (delta.magnitude / VortexDistance);

            if (dot < 0.0f)
            {

                    delta = localTree.points[index]-(PlayerFishSystem.playerPoint-(PlayerFishSystem.playerDir.normalized*2.0f));

                    groupingVector = - delta.normalized;
                    matchingVector = -delta.normalized;
            }
            else
            {
                if (distanceRatio < 0.8f)
                {


                    groupingVector = delta.normalized * dot;
                }
                
            }
            workSpeed[index] = Mathf.Lerp(speed, 5.0f, distanceRatio);
        }
        else
        {

            workSpeed[index] = speed;
            for (int j = indiceIndex - 10; (j < localTree.indices.Length) && (j < indiceIndex + 10); j++)
            {


                delta = (localTree.points[index] - localTree.points[localTree.indices[j]]);

                if (delta.sqrMagnitude < 130.0f)
                {

                    //attractiveness
                    proximityCounter++;
                    if (attractiveness[localTree.indices[j]] > attractiveness[index])
                    {
                        aModifier = 1.0f + attractiveness[localTree.indices[j]] - attractiveness[index];
                    }
                    else
                    {
                        aModifier = 1.0f - attractiveness[index] - attractiveness[localTree.indices[j]];

                    }
                    groupingVector -= delta;//(localTree.points[i] + -delta);
                    matchingVector += workVectors[localTree.indices[j]] * aModifier;
                }
            }
            groupingVector = (groupingVector / (proximityCounter));// -localTree.points[i];

        }

        workTargetVectors[index] = (workRepelVectors[index]) +
                                 groupingVector +
                                 (matchingVector * matchingVectorModifier);
    }
    public void PreSimulate()
    {
        localTree.SyncTree();
        localTree.HeapFullTest();

        for (int i = 0; i < totalFish;i++ )
        {
            proximityCounter = 0;
            //fishVectors[i] = Vector3.zero;

            indiceIndex = localTree.invertedIndices[i];
            if (indiceIndex < 10)
            {
                indiceIndex = 10;
            }


            for (int j = indiceIndex - 10; (j < localTree.indices.Length) && (j < indiceIndex + 10); j++)
            {


                delta = (localTree.points[i] - localTree.points[localTree.indices[j]]);

                if (delta.sqrMagnitude < 130.0f)
                {

                    //attractiveness

                    groupingVector -= delta;
                    //matchingVector += workVectors[localTree.indices[j]] * aModifier;
                }
            }
            groupingVector = (groupingVector / (proximityCounter));// -localTree.points[i];
            fishSystem.fishPoints[startIndex + i] += groupingVector;
        }
        

        

    }
    protected override void PushTransforms()
    {
        for (int i = 0; i < totalFish;i++ )
        {
            particles[i].position = fishSystem.fishPoints[startIndex + i];
           // particles[i].lifetime = 1.0f;
            
        }
        particleSystem.SetParticles(particles, particles.Length);

        //particleSystem.SetParticles(particles, particles.Length);
        //base.PushTransforms();
    }
}