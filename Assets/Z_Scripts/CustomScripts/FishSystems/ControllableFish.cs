﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class ControllableFish : MonoBehaviour
{
    public int DFSIndex;
    //public DFishSystem dfs;
    public DFish fishSystem;

    Transform cameraFollowerAnchor;
    CameraFollower cf;
    float cameraDistance = 28.0f;
    Vector3 motivation;

    Transform tail;
    ParticleSystem Bubbles;
    float idleSpeed = 2.0f;
    public float currentSpeed;
    float maxSpeed = 8.0f;
    bool speedBoostOn;
    bool speedBoostOff;

    float speedBoostTime = 0.6f;

    public float speedBoostTimer;

    public void Awake()
    {
    }
    public void Start()
    {
        motivation = new Vector3();


        cf = Camera.main.gameObject.AddComponent<CameraFollower>();
        if (cf != null)
        {
            cameraFollowerAnchor = new GameObject().transform;
            cameraFollowerAnchor.name = "_cameraFollowerAnchor";
            cameraFollowerAnchor.parent = transform.parent;
            cf.target = cameraFollowerAnchor;
            cf.lookAtTarget = transform;
            cf.distance = 1.0f;
        }
        gameObject.name = "MasterGoblin";
        if (renderer != null)
        {
            renderer.sharedMaterial.color = Color.red;

        }

        if (fishSystem != null)
        {
            fishSystem.InputAttractiveness(DFSIndex, 0.0f);
        }
       
        SphereCollider sp = gameObject.AddComponent<SphereCollider>();
        sp.radius = 1.0f;
        gameObject.AddComponent<Rigidbody>();
        rigidbody.isKinematic = true;

        GameObject g = (GameObject)Instantiate(Resources.Load("Prefabs/SpeedBubbles"));
        g.transform.parent = transform;
        g.transform.localPosition = Vector3.zero;
        g.transform.localEulerAngles = Vector3.zero;
        Bubbles = g.particleSystem;

        if (Bubbles != null)
        {
            Bubbles.enableEmission = false;
        }

    }
 
    public void Update()
    {


        if (speedBoostOn)
        {


            if (speedBoostTimer < speedBoostTime)
            {
                speedBoostTimer += Time.deltaTime ;

            }

        }
        if (speedBoostOff)
        {
            if (speedBoostTimer > 0.0f)
            {
                speedBoostTimer -= Time.deltaTime;
            }
            else
            {
                speedBoostOff = false;
            }
        }

        fishSystem.setSpeed(DFSIndex, Mathf.Lerp(idleSpeed,maxSpeed,speedBoostTimer/speedBoostTime));

        if (cameraFollowerAnchor != null)
        {
            //rotation
            if (Input.mousePresent&&!MinionController.isFocusing)
            {
                Vector3 cameraInput = new Vector3();
                cameraInput.x = (((Input.mousePosition.x / Screen.width) * 2) - 1.0f);
                cameraInput.y = (((Input.mousePosition.y / Screen.height) * 2) - 1.0f);

                cameraFollowerAnchor.RotateAround(transform.position, Vector3.up, 100 * cameraInput.x * Time.deltaTime);
                cameraFollowerAnchor.RotateAround(transform.position, -Camera.main.transform.right, 60 * cameraInput.y * Time.deltaTime);

            }
            


            //positioning
            Vector3 pos = cameraFollowerAnchor.position;
            pos.y = Mathf.Clamp(pos.y, transform.position.y - (cameraDistance * 0.5f), transform.position.y + (cameraDistance * 0.5f));

            Vector3 dir = (transform.position - cameraFollowerAnchor.position);

            //cameraFollowerAnchor.forward = Vector3.RotateTowards(cameraFollowerAnchor.forward, dir.normalized, 0.3f * Time.deltaTime, 1.0f);
            
            
            dir = dir - dir.normalized * cameraDistance;
            pos += dir * Time.deltaTime;

            cameraFollowerAnchor.position = pos;
        }
        
        GetInput();
    }

    float doubleTouchTimer;
    float timeSinceLastDoubleTap;
    bool doubletapBoost;
    int currentDoubleTapID;

    bool doubleTap;

    public bool toggleIdle;

    void GetInput()
    {

        if (Input.GetMouseButton(0))
        {
            motivation.x = Mathf.Clamp((((Input.mousePosition.x / Screen.width) * 2) - 1.0f)*1.1f,-1.0f,1.0f);
            motivation.y = Mathf.Clamp((((Input.mousePosition.y / Screen.height)  * 2) - 1.0f)*1.1f,-1.0f,1.0f);
            motivation.z = 1.0f-(Mathf.Max(Mathf.Abs(motivation.x), Mathf.Abs(motivation.y)));
            fishSystem.InputMotivation(DFSIndex, (Camera.main.transform.right * motivation.x) + (Camera.main.transform.up * motivation.y) + (Camera.main.transform.forward * motivation.z));
        }
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            if (scroll > 0.0f)
            {
                cameraDistance = Mathf.Clamp(cameraDistance - 5.0f, 8.0f, 40.0f);

            }
            else
            {
                cameraDistance = Mathf.Clamp(cameraDistance + 5.0f, 8.0f, 40.0f);
                
            }
        }
   
        


        if (Input.touchCount == 2)
        {
            if ( currentDoubleTapID >=1 && !doubletapBoost)
            {
                EnterSpeedBoost();
                doubletapBoost = true;
            }
            //timeSinceLastDoubleTap = 0.0f;
            doubleTap = true;
            doubleTouchTimer += Time.deltaTime;
            
        }

        if (Input.touchCount != 2)
        {
            if (doubleTap)
            {
                timeSinceLastDoubleTap += Time.deltaTime;

            }
            if (timeSinceLastDoubleTap >= 0.3f)
            {
                currentDoubleTapID = 0;
                timeSinceLastDoubleTap = 0.0f;
                doubleTap = false;
            }

            if (doubletapBoost)
            {
                LeaveSpeedBoost();
                doubletapBoost = false;
            }

            if (doubleTap && doubleTouchTimer > 0.0f && doubleTouchTimer < 0.2f)
            {
                timeSinceLastDoubleTap = 0.0f;
                doubleTouchTimer = -0.01f;
                currentDoubleTapID++;
                //double touch tap released
            }

            doubleTouchTimer = 0.0f;
            //doubleTap = false;
            
        }
        if (Input.GetMouseButtonDown(0))
        {
           // Debug.Log("SPEED!");

            EnterSpeedBoost();
        }
        if (Input.GetMouseButtonUp(0))
        {
            LeaveSpeedBoost();
            

        }
    }
    void EnterSpeedBoost()
    {
        speedBoostOn = true; 
        speedBoostOff = false;

        if (Bubbles != null)
        {
            Bubbles.enableEmission = true;

        }

        //fishSystem.setSpeed(DFSIndex, maxSpeed);
    }
    void LeaveSpeedBoost()
    {
        if (Bubbles != null)
        {
            Bubbles.enableEmission = false;

        }
        speedBoostOn = false;
        speedBoostOff = true;
        //fishSystem.setSpeed(DFSIndex, defaultSpeed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Egg")
        {
            Destroy(other.gameObject);
        }
    }
    void OnGUI()
    {
        GUI.Label(new Rect(0, 350, 200, 200), "ttttime " + speedBoostTimer.ToString());

        /*if (Input.GetMouseButton(0))
        {
            GUI.Label(new Rect(0, 300, 200, 200), "Mouse active "+motivation.ToString());
        }
        GUI.Label(new Rect(0, 350, 200, 200), "doubleTapTime " + doubleTouchTimer.ToString());
        GUI.Label(new Rect(0, 375, 200, 200), "time since last doubletap " + timeSinceLastDoubleTap.ToString());
        GUI.Label(new Rect(0, 400, 200, 200), "doubletapboos " + doubletapBoost.ToString());
        GUI.Label(new Rect(0, 440, 200, 200), "doubletapID " + currentDoubleTapID.ToString());*/

    }
}
