﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class ParticleFish : PlanktonSystem
{
    protected override void PushTransforms()
    {
        for (int i = 0; i < totalFish; i++)
        {
            

            delta =  particles[i].position;
            particles[i].position = fishSystem.fishPoints[startIndex + i];
            delta =  particles[i].position - delta;
            if (delta != Vector3.zero)
            {
                particles[i].axisOfRotation = delta.normalized;
            }

        }
        particleSystem.SetParticles(particles, particles.Length);


    }
}