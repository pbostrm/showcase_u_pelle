﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Threading;

public class DSpawnArea : MonoBehaviour
{
    public List<DQueryLocation> locations = new List<DQueryLocation>();

    public Color GizmoColor = Color.yellow;

   // [HideInInspector]
    public bool BuildingArea;

    public string DOctreeName;
    public string SpawnName;
    Vector3 pos;
    public void OnDrawGizmos()
    {
        if (locations != null)
        {
            Gizmos.color = GizmoColor;
            foreach (var loc in locations)
            {
                if (loc.position != Vector3.zero && loc.radius > 0.0f)
                {
                    Gizmos.DrawWireSphere(transform.position + loc.position, loc.radius);
                }
            }
        }
    }

    public void BuildSpawnArea()
    {
        pos = transform.position;
      //  Thread t = new Thread(new ThreadStart(BuildProcess));
        BuildProcess();
        //t.Start();
    }
    void BuildProcess()
    {
        BuildingArea = true;
        Debug.Log("Building spawn area t");
        DOctree doctree = new DOctree(DOctreeName, pos);
        DQueryArea.BuildAndSaveSpawnArea("spawn/" + SpawnName, doctree, locations,pos);

        
        doctree = null;
        GC.Collect();
        BuildingArea = false;
    }
}