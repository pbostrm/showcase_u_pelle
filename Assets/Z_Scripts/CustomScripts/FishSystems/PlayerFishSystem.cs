﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
class PlayerFishSystem : DFish
{
    public static PlayerFishSystem player;

    public Transform playerTransform;
    public Vector3 point;
    public Vector3 dir;

    public bool PlayableFish = true;
    static public Vector3 playerPoint
    {
        get
        {
            return player.point;
        }
    }
    static public Vector3 playerDir
    {
        get
        {
            return player.dir;
        }
    }
    public void Awake()
    {
        player = this;
        base.Awake();

    }
    public void Update()
    {
        base.Update();
        dir = fishSystem.fishVectors[startIndex];
        point = fishSystem.fishPoints[startIndex];
        playerTransform = transformPool[0];
    }
    public override void SpawnFish()
    {
        base.SpawnFish();

        if (PlayableFish)
        {
            //fishSystem.fishMovementSpeed[0] = 4.5f;
            ControllableFish cf = transformPool[0].gameObject.AddComponent<ControllableFish>();
            transformPool[0].gameObject.AddComponent<MinionTargetComponent>();
            if (cf != null)
            {
                cf.fishSystem = this;
                cf.DFSIndex = startIndex;
            }
            if (SceneSystem.currentActiveSceneSystem != null)
            {
                SceneSystem.currentActiveSceneSystem.ActiveFishInScene = activeFish;

            }
        }
    }
}
