﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FishPool : MonoBehaviour
{
    static List<GameObject> fishPool;
    static public int FishInWorldCnt = 0;
    static public int nonPooledFishCnt = 0;
    static public int PooledFishCnt = 0;
    static public int maxPooledFish = 100;

    public static FishPool fishPoolGuiObject;

    public static void AddFishToPool(GameObject go)
    {
        
        if (fishPool == null)
        {
            fishPool = new List<GameObject>();
        }
        if (fishPool != null)
        {
            fishPool.Add(go);
        }
    }

    public static void CreateFish(Transform parent,GameObject fishPrefab, int count, bool superSchool)
    {

        if (fishPool == null)
        {
            if (fishPoolGuiObject == null)
            {
                GameObject g = new GameObject("_FishPoolObject");
                fishPoolGuiObject = g.AddComponent<FishPool>();
            }
            fishPool = new List<GameObject>();
        }

        for (int i = 0; i < count; i++)
        {
            GameObject g = (GameObject)Instantiate(fishPrefab);
            g.transform.parent = parent;
            fishPool.Add(g);

           
            FishCulling FC = g.GetComponent<FishCulling>();
            if (FC != null)
            {
                 if (superSchool) //the fish represents a group of fish
                {
                    if (FC != null)
                    {
                        FC.SchoolingSize = FC.maxSchoolingSize;
                    }
                }
                else
                {
                    FC.SchoolingSize = 1;
                }
                PooledFishCnt += FC.SchoolingSize;
            }
            
            g.SetActive(false);
        }
        
    }
    public static void SpawnFromPool(Transform parent,Vector3 position,int count)
    {
        
        for (int i = 0; i < count; i++)
        {
            if (fishPool.Count >= 1)
            {
                nonPooledFishCnt++;
                PooledFishCnt--;
                fishPool[0].transform.parent = parent;
                fishPool[0].transform.position = position;
                fishPool[0].SetActive(true);
                fishPool.RemoveAt(0);
            }
        }
    }
    public static void PoolFish(FishControl fish)
    {
        nonPooledFishCnt--;
        PooledFishCnt++;
        fish.fishCulling.SchoolingSize = 1;
        fish.gameObject.SetActive(false);
        fishPool.Add(fish.gameObject);
    }
    public void OnGUI()
    {
        GUIStyle gs = new GUIStyle();
        gs.alignment = TextAnchor.MiddleRight;
        gs.normal.textColor = Color.yellow;

        float width = 100;
        GUI.Label(new Rect(Screen.width - width, 20, width - 10, 15), "pooled Fish objects: " + fishPool.Count, gs);
        GUI.Label(new Rect(Screen.width - width, 35, width - 10, 15), "pooled Fish cnt: " + PooledFishCnt, gs);
        GUI.Label(new Rect(Screen.width - width, 50, width - 10, 15), "nonpooled Fish: " + nonPooledFishCnt, gs);
        GUI.Label(new Rect(Screen.width - width, 65, width - 10, 15), "total Fish: " + (nonPooledFishCnt + PooledFishCnt), gs);
    }
}
