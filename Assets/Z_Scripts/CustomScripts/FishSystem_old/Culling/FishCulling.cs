﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FishCulling : FishBehaviour
{
    public int SchoolingSize = 1;
    public int maxSchoolingSize = 20;
    public bool outsideCullerRange = true;

    bool lastZoneSide;

    public GameObject fishPrefab;

    public void Update()
    {
        outsideCullerRange = false;
        if (FishCuller.fishCullers != null)
        {
            foreach (var fishCuller in FishCuller.fishCullers)
            {
                if ((transform.position - fishCuller.transform.position).sqrMagnitude > fishCuller.radius * fishCuller.radius)
                {
                    outsideCullerRange = true;
                    break;
                }
            }
            if (outsideCullerRange && !lastZoneSide)
            {
                LeaveZone();
            }
            if (!outsideCullerRange && lastZoneSide)
            {
                EnterZone();
            }
            lastZoneSide = outsideCullerRange;

            
        }
        if (SchoolingSize >= maxSchoolingSize)
        {
            //schooling.GroupUpVelocity = Vector3.zero;
            //schooling.MatchVelocity = Vector3.zero;

        }
        
    }
    public void EnterZone()
    {
        if (SchoolingSize > 1)
        {
            //DebugOutput.Shout("Creating fish : " + SchoolingSize);
            FishPool.SpawnFromPool(transform.parent, transform.position, SchoolingSize - 1);
            SchoolingSize = 1;
        }
    }
    public void LeaveZone()
    {

        if (schooling != null)
        {
            if (schooling.closestFishes != null)
            {
                for (int i = schooling.closestFishes.Count - 1; i >= 0; i--)
                {
                    if (schooling.closestFishes[i].fishCulling.outsideCullerRange)
                    {
                        SchoolingSize += schooling.closestFishes[i].fishCulling.SchoolingSize;
                        FishPool.PoolFish(schooling.closestFishes[i]);
                        schooling.closestFishes[i].fishCulling.SchoolingSize = 1;

                        schooling.closestFishes.RemoveAt(i);
                    }
                }
            }

        }
    }
    public void OnDrawGizmos()
    {
        if (outsideCullerRange)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }

        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}
