﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FishCuller : MonoBehaviour
{
    public static List<FishCuller> fishCullers;

    public float radius = 10.0f;

    public void Awake()
    {
        if(fishCullers == null)
        {
            fishCullers = new List<FishCuller>();
        }
        if (!fishCullers.Contains(this))
        {
            fishCullers.Add(this);
        }
        DebugOutput.Shout("this is how many fishcullers in the list: " + fishCullers.Count);
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.5f, 0.5f, 1.0f);
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
