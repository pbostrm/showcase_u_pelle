﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class BoundsContainment  : FishBehaviour
{

    public static List<BoundsOctree> bounds;
    public BoundsOctree baseOctree;
    public BoundsOctree boundsOctree;
    public float updateInterval = 0.2f;
    public float updateTimer;

    private bool ObstacleAwareness;
    private bool newDirection;
    private Vector3 overrideDirection;

    public float scanRange = 0.5f;
    public Vector3 hitNormal;
    private RaycastHit hit;

    public Vector3 oldPos;

    public bool outsideBounds;

    public bool gotBounds = true;
    public void Awake()
    {
        updateTimer = UnityEngine.Random.Range(0.0f, updateInterval);

        hit = new RaycastHit();
    }
    public void Update()
    {
        if (boundsOctree == null)
        {
            boundsOctree = GetClosestBounds( transform.position);

            if (boundsOctree == null)
            {
                outsideBounds = true;
                gotBounds = false;

            }
        }

        if (boundsOctree != null)
        {
            if (!boundsOctree.isPointInside(transform.position) || boundsOctree.Obstacle)
            {
                boundsOctree = null;
            }
            else
            {
                oldPos = transform.position;

            }
            outsideBounds = false;
        }
        if (outsideBounds)
        {
            schooling.Overridden = true;
            fishMove.targetDirection = oldPos -transform.position;
            fishMove.moveDirection = fishMove.targetDirection;
        }
        else
        {
            schooling.Overridden = false;
        }
    
    }

    static public void AddBounds(BoundsOctree bound)
    {
        if (bounds == null)
        {
            bounds = new List<BoundsOctree>();
            
        }
        if (!bounds.Contains(bound))
        {
            bounds.Add(bound);
        }
    }
    static public BoundsOctree GetClosestBounds(Vector3 pos)
    {
        if (bounds != null)
        {
            foreach (var bound in bounds)
            {
                if (bound.isPointInside(pos))
                {
                    return bound.GetBound(pos);
                }
            }
        /*    float minDistance = (pos - bounds[0].position).sqrMagnitude;
            BoundsOctree bo = bounds[0];
            foreach (var bound in bounds)
            {
                float distance = (pos - bound.position).sqrMagnitude;
                if (distance <= minDistance)
                {
                    minDistance = distance;
                    bo = bound;
                }
            }
            return bo;*/
        }
        return null;
    }
#if UNITY_EDITOR
    public override void sDrawGizmos()
    {
        if (boundsOctree != null)
        {
            Gizmos.DrawCube(boundsOctree.position,Vector3.one*boundsOctree.size*2.0f);

            Gizmos.color = new Color(0.1f,0f,0.8f,0.2f);
            if (boundsOctree.neighbors != null)
            {
                foreach (var neighbor in boundsOctree.neighbors)
                {

                    Gizmos.DrawCube(neighbor.position, (Vector3.one * neighbor.size * 2) - Vector3.one * 0.1f);

                }
            }
            
        }
        return;

    }
#endif

}