﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FishBehaviour : sBehaviour
{
    FishControl pFC;
    public FishControl fishControl
    {
        get
        {
            if (pFC == null)
            {
                pFC = GetComponent<FishControl>();
            }
            return pFC;   
        }
    }

    Schooling pSchooling;
    public Schooling schooling
    {
        get
        {
            if (pSchooling == null)
            {
                pSchooling = GetComponent<Schooling>();
            }
            return pSchooling;
        }
    }

    FishCulling pFCulling;
    public FishCulling fishCulling
    {
        get
        {
            if (pFCulling == null)
            {
                pFCulling = GetComponent<FishCulling>();
            }
            return pFCulling;
        }
    }

    ProximityControl pPC;
    public ProximityControl proximityControl
    {
        get
        {
            if (pPC == null)
            {
                pPC = GetComponent<ProximityControl>();
            }
            return pPC;
        }
    }

    AttractivenessModule pAM;
    public AttractivenessModule attractivenessModule
    {
        get
        {
            if (pAM == null)
            {
                pAM = GetComponent<AttractivenessModule>();
            }
            return pAM;
        }
    }

    FishMove pFishMove;
    public FishMove fishMove
    {
        get
        {
            if (pFishMove == null)
            {
                pFishMove = GetComponent<FishMove>();
            }
            return pFishMove;
        }
    }

    BoundsContainment pBC;
    public BoundsContainment boundsContainment
    {
        get
        {
            if (pBC == null)
            {
                pBC = GetComponent<BoundsContainment>();
            }
            return pBC;
        }
    }
   /* void Awake()
    {
        Init();
    }

    public virtual void Init()
    {
    }*/
}