﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CameraFollower : MonoBehaviour
{
    public Transform target;
    public Transform lookAtTarget;
    Vector3 lookAtFollowPoint;
    public float distance;
    public void Awake()
    {
        CameraControl control = GetComponent<CameraControl>();
        if (control != null)
        {
            control.enabled = false;
        }
    }

    public void Update()
    {
        if (target != null)
        {
            Vector3 delta = (lookAtTarget.position-lookAtFollowPoint);
            lookAtFollowPoint = lookAtTarget.position - (delta.normalized * Mathf.Clamp(delta.magnitude - ((delta.magnitude-2.0f)*Time.deltaTime)
                ,1.0f,4.0f));

            Vector3 pos = transform.position;
            pos.y = Mathf.Clamp(pos.y, target.position.y - (distance * 0.5f), target.position.y + (distance * 0.5f));

            Vector3 dir = (target.position - transform.position);

            Vector3 lookatDir = (lookAtFollowPoint - transform.position);
            
            transform.forward = lookatDir.normalized;

            dir =dir - dir.normalized * distance;
            pos += dir * Time.deltaTime;

            transform.position = pos;

        }
    }
}
