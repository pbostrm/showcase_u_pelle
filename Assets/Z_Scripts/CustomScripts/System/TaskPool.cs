﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

public class TaskPool : MonoBehaviour
{
    static GameObject taskPoolObject;
    
    public delegate void Task();

    static public List<Task> tasks;
    static public List<Job> JobQueue;
    static public List<Job> postJobQueue;
    public List<Task> postJobs;
    static public Worker[] workers;
    static public ReaderWriterLockSlim rwLock;

    public int jobQueuecnt;
    public int postJobQueueCnt;
    public bool working = false;

    public int OccupiedWorkers;
    public void OnDestroy()
    {
         //SHUT... DOWN... EVERYTHING...
        if (workers != null)
        {
            for (int i = 0; i < 8; i++)
            {
                workers[i].workerThread.Abort();

            }
        }
        
    }
    public void Update()
    {
        
        if(postJobs==null)
        {
            postJobs = new List<Task>();
        }
        working = false;
        rwLock.EnterUpgradeableReadLock();

      /*  if (workers != null)
        {
            OccupiedWorkers = 0;
            foreach (var worker in workers)
            {
                if (worker.activeJob != null)
                {
                    OccupiedWorkers++;
                }
            }
        }*/
        try
        {
           // working = true;
            //jobQueuecnt = JobQueue.Count;
            //postJobQueueCnt = postJobQueue.Count;
            if (postJobQueue != null && postJobQueue.Count >= 1)
            {
                for (int i = postJobQueue.Count - 1; i >= 0; i--)
                {
                   
                    rwLock.EnterWriteLock();
                    try
                    {
                        if (postJobQueue[i].Post != null)
                        {
                            postJobs.Add(postJobQueue[i].Post);


                            //DebugOutput.Shout("Post!");
                        }
                        postJobQueue.RemoveAt(i);
                    }
                    finally
                    {
                        rwLock.ExitWriteLock();
                    }
                    
                    
                }
            }
        }
        finally
        {
            rwLock.ExitUpgradeableReadLock();
        }
        //DebugOutput.Shout(postJobs.Count.ToString());

        for (int i = postJobs.Count - 1; i >= 0; i--)
        {
            postJobs[i]();
            postJobs.RemoveAt(i);
            //if(i == 0)
            
        }
        //postJobs.Clear();
       
    }
    static public void QueueTask(Task task,Task post)
    {
        if (taskPoolObject == null)
        {
            taskPoolObject = new GameObject("_taskPoolObject");
            taskPoolObject.AddComponent<TaskPool>();

            JobQueue = new List<Job>();
            postJobQueue = new List<Job>();
            rwLock = new ReaderWriterLockSlim();

            tasks = new List<Task>();

            if (workers == null)
            {
                workers = new Worker[8];
                for (int i = 0; i < 8; i++)
                {
                    workers[i] = new Worker();
                    workers[i].workerThread = new Thread(new ThreadStart(workers[i].Work));
                    workers[i].workerThread.Start();


                }
                Debug.Log("started 4 Workers");
            }
        }
        Job InsertJob = new Job(task,post);
        rwLock.EnterUpgradeableReadLock();
        try
        {
            if (JobQueue == null)
            {
                rwLock.EnterWriteLock();
                try
                {
                    JobQueue = new List<Job>();

                }
                finally
                {
                    rwLock.ExitWriteLock();

                }
            }
            if (JobQueue != null)
            {

                rwLock.EnterWriteLock();
                try
                {
                    JobQueue.Add(InsertJob);
                }
                finally
                {
                    rwLock.ExitWriteLock();

                }
            }
        }
        finally
        {
            rwLock.ExitUpgradeableReadLock();
        }  

    }
    static public Job getJob()
    {
        Job prioJob = null;

        rwLock.EnterUpgradeableReadLock();
        try
        {
            if (JobQueue.Count >= 1)
            {

                rwLock.EnterWriteLock();
                try
                {
                    prioJob = JobQueue[0];
                    JobQueue.RemoveAt(0);
                }
                finally
                {
                    rwLock.ExitWriteLock();

                }
            }
        }
        finally
        {
            rwLock.ExitUpgradeableReadLock();

        }
        return prioJob;
    }
    static public Task getTask()
    {

        Task prioTask = null;
        rwLock.EnterUpgradeableReadLock();
        try
        {
            if (tasks.Count >= 1)
            {

                rwLock.EnterWriteLock();
                try
                {
                    prioTask = tasks[0];
                    tasks.RemoveAt(0);
                }
                finally
                {
                    rwLock.ExitWriteLock();

                }
            }
        }
        finally
        {
            rwLock.ExitUpgradeableReadLock();

        }
        return prioTask;
    }
    static public void QueuePostJob(Job j)
    {
        rwLock.EnterUpgradeableReadLock();
        try
        {
            if (postJobQueue == null)
            {

                rwLock.EnterWriteLock();
                try
                {
                    postJobQueue = new List<Job>();
                }
                finally
                {
                    rwLock.ExitWriteLock();

                }
            }
            rwLock.EnterWriteLock();
            try
            {
                postJobQueue.Add(j);
            }
            finally
            {
                rwLock.ExitWriteLock();

            }
        }
        finally
        {
            rwLock.ExitUpgradeableReadLock();

        }
    }
}
public class Worker
{
    public Thread workerThread;
    TaskPool.Task task;
    public Job activeJob;
    public bool run = true;
    public Worker()
    {

    }

    public void Work()
    {
        while (run)
        {
            if (activeJob != null)
            {
                activeJob.Main();
                
                TaskPool.QueuePostJob(activeJob);
                //activeJob = null;
            }

            activeJob = TaskPool.getJob();
            if (activeJob == null)
            {
                Thread.Sleep(1);
            }
        }
    }
}
public class Job
{
    public TaskPool.Task Pre;
    public TaskPool.Task Main;
    public TaskPool.Task Post;

    public Job(TaskPool.Task main)
    {
        Pre = null;
        Main = main;
        Post = null;
    }
    public Job( TaskPool.Task main, TaskPool.Task post)
    {
        Pre = null;
        Main = main;
        Post = post;
    }
    public Job(TaskPool.Task pre, TaskPool.Task main, TaskPool.Task post)
    {
        Pre = pre;
        Main = main;
        Post = post;
    }
}