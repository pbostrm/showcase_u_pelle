﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Diagnostics;

public class DOPtests : MonoBehaviour
{
    int[] nodeToRight;
    int[] nodeToLeft;

    int[] childsTotal;
    bool[] obstacleNode;
    bool[] borderNode;
    Vector3[] borderUnDirection;
    float[,] obstacleOutDirection;
    int[] parent;
    int[] levelDepth;
    int[,] binaryLocation;
    int[] cornerID;
    public string BoundsName;
    public bool loadFromFile;
    float size;
    int maxLevelDepth;
    int nodeCount;

    int[,] target; //used for finding neighbors;

    int currentNode = 7768;

    static public  DOPtests bounds;
    public bool StressTest = false;
    //Borders! Borders contain exit vector(calculated).

    #region properties
    public static readonly short[,] AdjacentDirections = new short[26, 3]
	{
		{1,0,0},
		{-1,0,0},

		{1,1,0},
		{-1,-1,0},
		{-1,1,0},
		{1,-1,0},

		{1,0,1},
		{-1,0,-1},
		{1,0,-1},
		{-1,0,1},

		{1,1,1},
		{-1,1,1},
		{-1,-1,1},
		{-1,-1,-1},
		{1,-1,-1},
		{1,1,-1},
		{1,-1,1},
		{-1,1,-1},
		
		{0,1,1},
		{0,-1,-1},
		{0,-1,1},
		{0,1,-1},

		{0,0,1},
		{0,0,-1},

		{0,1,0},
		{0,-1,0}
	};

    #endregion
    Vector3[] corners;
        
    void Awake()
    {
        corners = new Vector3[8];
        corners[0] = new Vector3(0f, 0f, 0f);
        corners[1] = new Vector3(1f, 0f, 0f);
        corners[2] = new Vector3(0f, 0f, 1f);
        corners[3] = new Vector3(1f, 0f, 1f);
        corners[4] = new Vector3(0f, 1f, 0f);
        corners[5] = new Vector3(1f, 1f, 0f);
        corners[6] = new Vector3(0f, 1f, 1f);
        corners[7] = new Vector3(1f, 1f, 1f);


        if (BoundsName != null && loadFromFile)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //----
            LoadBoundsFromFile("OctreeData/DOP_"+BoundsName);
            BuildData();

            //---
            watch.Stop();

            DebugOutput.Shout("loading bounds DOPmode took " +
                watch.ElapsedMilliseconds.ToString() + " milliseconds to process");

           // RenderNodes();
            NodePrint(0);

            NodePrint(5457);
            FindNeighBors(5457);
            RenderNode(FindNode(new Vector3(25.035f, 25.880f, -23.500f)),Color.blue);


            if (StressTest)
            {
#region stressTest
                watch.Reset();
                watch.Start();
                int neighborCheckCnt = 0;
                for (int j = 0; j < 1; j++)
                {
                    for (int i = 0; i < nodeCount; i++)
                    {
                        if (childsTotal[i] == 0)
                        {
                            neighborCheckCnt++;
                            FindNeighBors(i);
                        }
                    }
                }
                watch.Stop();

                DebugOutput.Shout("getting " + neighborCheckCnt.ToString() + " took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process");

                watch.Reset();
                watch.Start();
                Vector3 randomPoint = new Vector3();

                for (int j = 0; j < 100000; j++)
                {
                    randomPoint.x = UnityEngine.Random.Range(-40.0f, 40.0f);
                    randomPoint.y = UnityEngine.Random.Range(-40.0f, 40.0f);
                    randomPoint.z = UnityEngine.Random.Range(-40.0f, 40.0f);
                    FindNode(randomPoint);
                }
                watch.Stop();

                DebugOutput.Shout("finding random point took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process");
#endregion

                watch.Reset();
                watch.Start();

                int[] neighborDirection = new int[3];

                int neighborCount = 0;
                Vector3 outDirection = new Vector3();
                Vector3 inDirection = new Vector3();

                int pasted = 3;
                Vector3 adjacentDirection = new Vector3();

                int counter = 0;
                for (int i = 1; i < nodeCount; i++)
                {
                    neighborCount = 0;
                    if (childsTotal[i] == 0 && obstacleNode[i])
                    {
                        for (int j = 0; j < 26;j++ )
                        {
                            Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
                            float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
                            pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

                            pos = ((pos * 2.0f) - Vector3.one * size) + transform.position;
                            pos = pos + (Vector3.one * 0.5f) * nodesize;
                            adjacentDirection[0] = AdjacentDirections[j,0];
                            adjacentDirection[1] = AdjacentDirections[j,1];
                            adjacentDirection[2] = AdjacentDirections[j,2];

                            int adjacentNode = FindNode(pos + (adjacentDirection*nodesize));
                            if (adjacentNode > 0 && adjacentNode != i && !obstacleNode[adjacentNode])
                            {
                                UnityEngine.Debug.DrawLine(pos, pos + adjacentDirection, Color.red, 50.0f);
                                counter++;
                            }
                           
                        }
                    }
                }

                watch.Stop();

                DebugOutput.Shout("bruteforce took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process "+counter.ToString());
                /*for (int i = 1;i < nodeCount; i++)
                {
                    neighborCount = 0;
                    if (childsTotal[i] == 0 && obstacleNode[i])
                    {

                        outDirection = Vector3.zero;
                        inDirection = Vector3.zero;
                        for (int j = 1; j < nodeCount; j++)
                        {
                            if (j != i && childsTotal[j] == 0 && !obstacleNode[j] &&
                                !((cornerID[i]==0&&cornerID[j]==3)||
                                (cornerID[i]==1&&cornerID[j]==2)||
                                (cornerID[i]==2&&cornerID[j]==1)||
                                (cornerID[i]==3&&cornerID[j]==0)||

                                (cornerID[i]==4&&cornerID[j]==7)||
                                (cornerID[i]==5&&cornerID[j]==6)||
                                (cornerID[i]==6&&cornerID[j]==5)||
                                (cornerID[i]==7&&cornerID[j]==4)||

                                (cornerID[i]==5&&cornerID[j]==0)||
                                (cornerID[i]==0&&cornerID[j]==5)||
                                (cornerID[i]==1&&cornerID[j]==4)||
                                (cornerID[i]==4&&cornerID[j]==1)||

                                (cornerID[i]==2&&cornerID[j]==7)||
                                (cornerID[i]==7&&cornerID[j]==2)||
                                (cornerID[i]==3&&cornerID[j]==6)||
                                (cornerID[i]==6&&cornerID[j]==3)||

                                (cornerID[i]==2&&cornerID[j]==4)||
                                (cornerID[i]==4&&cornerID[j]==2)||
                                (cornerID[i]==3&&cornerID[j]==5)||
                                (cornerID[i]==5&&cornerID[j]==3)||

                                (cornerID[i]==4&&cornerID[j]==3)||
                                (cornerID[i]==3&&cornerID[j]==4)||
                                (cornerID[i]==0&&cornerID[j]==7)||
                                (cornerID[i]==7&&cornerID[j]==0)||

                                (cornerID[i]==1&&cornerID[j]==6)||
                                (cornerID[i]==6&&cornerID[j]==1)||
                                (cornerID[i]==2&&cornerID[j]==5)||
                                (cornerID[i]==5&&cornerID[j]==2)
                                )
                                )
                            {
                                for(int k = 0; k < 3;k++)
                                {
                                    if (binaryLocation[j, k] - 1 < binaryLocation[i, k] + (1 << levelDepth[i])   //if this node is bordering a,b,+b_size,c
                                    && binaryLocation[j, k] + (1<<levelDepth[j])  >= binaryLocation[i, k] )
                                    {
                                        if(binaryLocation[j,k] >= binaryLocation[i, k] + (1 << levelDepth[i]))
                                        {
                                            obstacleOutDirection[i,k] = 1.0f;// Mathf.Pow(levelDepth[0] - levelDepth[j], 2.0f);
                                        }
                                        else if(binaryLocation[j,k] +(1<<levelDepth[j])-1 < binaryLocation[i,k])
                                        {
                                            obstacleOutDirection[i,k+3] = -1.0f;// Mathf.Pow(levelDepth[0] - levelDepth[j], 2.0f);
                                        } 
                                           //need to check if J is corner
                                        if(k == 2)
                                        {
                                            neighborCount++;
                                            borderNode[i] = true;
                                        }
                                    }
                                    else
                                    {
                                        break; 
                                    }
                                }
                            }
                        }

                        if (borderNode[i])
                        {
                           // borderUnDirection[i].Normalize();

                            Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
                            float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
                            pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

                            pos = ((pos * 2.0f) - Vector3.one * size) + transform.position;
                            pos = pos + (Vector3.one * 0.5f) * nodesize;



                            for (int k = 0; k < 6; k++)
                            {
                                if (pasted>=0)
                                {
                                    DebugOutput.Shout("Banansasa " + obstacleOutDirection[i, k]);
                                    if (obstacleOutDirection[i, k] != 0)
                                    {
                                        Vector3 banansa = new Vector3();
                                        banansa[k % 3] = obstacleOutDirection[i, k];
                                        //banansa.Normalize();
                                        UnityEngine.Debug.DrawLine(pos, pos + banansa, Color.red, 50.0f);
                                        // UnityEngine.Debug.DrawLine(pos + (Vector3.one * 0.5f) * nodesize, (pos + (Vector3.one * 0.5f) * nodesize) + Vector3.up, Color.blue, 50.0f);

                                    }
                                }
                                
                            }
                            if (pasted >= 0)
                            {
                                DebugOutput.Shout("==============================");
                            }
                            pasted--;
                        }
                    }
                    if (obstacleNode[i] && childsTotal[i] == 0)
                    {
                        RenderNode(i,Color.grey);
                    }
                }
                watch.Stop();

                DebugOutput.Shout("bruteforce took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process");
                 */
            }
           
            
        }
    }

    void LoadBoundsFromFile(string fileName)
    {
        TextAsset ta = (TextAsset)Resources.Load(fileName);
        if (ta.bytes.Length != null)
        {
            BinaryReader br = new BinaryReader(new MemoryStream(ta.bytes));
            
            size = (float)br.ReadDouble();
            maxLevelDepth = br.ReadInt32();
            nodeCount = br.ReadInt32();
            DebugOutput.Shout("There is "+nodeCount+" nodes in the file, maxLevelDepth= "+maxLevelDepth.ToString()+" size: "+size.ToString());

            childsTotal = new int[nodeCount];
            nodeToRight = new int[nodeCount];
            nodeToLeft = new int[nodeCount];
            obstacleNode = new bool[nodeCount];

            for (int i = 0; i < nodeCount;i++ )
            {
                childsTotal[i] = br.ReadInt32()-1;
                nodeToRight[i] = i+childsTotal[i]+1;
                obstacleNode[i] = br.ReadBoolean();
            }
        }
    }


    void BuildData()
    {
        parent = new int[nodeCount];
        levelDepth = new int[nodeCount];
        binaryLocation = new int[nodeCount, 3];
        cornerID = new int[nodeCount];

        levelDepth[0] = maxLevelDepth;

        //making leveldepth and parent hierarchy
        for (int i = 0; i < nodeCount; i++)
        {
            for (int j = i + 1; j < nodeToRight[i]; j++)
            {
                parent[j] = i;
                levelDepth[j] = levelDepth[i] - 1;
            }

        }
        //creating left and right pointers
        for (int i = 1; i < nodeCount; i++)
        {
            if (nodeToRight[i] < nodeCount && nodeToRight[parent[i]] != nodeToRight[i])
            {
                nodeToLeft[nodeToRight[i]] = i;
            }

        }

        //Building corners and binary locations
        for (int i = 0; i < nodeCount; i++)
        {
            if (nodeToLeft[i] == 0)
            {
                cornerID[i] = 0;
            }
            else
            {
                cornerID[i] = cornerID[nodeToLeft[i]] + 1;
            }
            if (i > 0)
            {
                binaryLocation[i, 0] = binaryLocation[parent[i], 0];
                binaryLocation[i, 1] = binaryLocation[parent[i], 1];
                binaryLocation[i, 2] = binaryLocation[parent[i], 2];


                if (cornerID[i] % 2 != 0)
                {
                    binaryLocation[i, 0] = binaryLocation[parent[i], 0] + (int)Math.Pow(2, levelDepth[i]);
                }
                if (cornerID[i] > 3)
                {
                    binaryLocation[i, 1] = binaryLocation[parent[i], 1] + (int)Math.Pow(2, levelDepth[i]);

                }
                if (cornerID[i] == 2 || cornerID[i] == 3 || cornerID[i] == 6 || cornerID[i] == 7)
                {
                    binaryLocation[i, 2] = binaryLocation[parent[i], 2] + (int)Math.Pow(2, levelDepth[i]);

                }
            }
        }
        target = new int[26, 3];

        borderNode = new bool[nodeCount];
        borderUnDirection = new Vector3[nodeCount];

        for (int i = 1; i < nodeCount; i++)
        {
            if (childsTotal[i] == 0)
            {

            }

        }
        obstacleOutDirection = new float[nodeCount,6];
    }

    public bool checkObstacle(Vector3 point)
    {
        int cnode = FindNode(point);

        if (cnode > 0 && obstacleNode[cnode])
        {
            return true;
        }
        return false;
        /*for (int j = 0; j < 26; j++)
        {
            Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
            float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
            pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

            pos = ((pos * 2.0f) - Vector3.one * size) + transform.position;
            pos = pos + (Vector3.one * 0.5f) * nodesize;
            adjacentDirection[0] = AdjacentDirections[j, 0];
            adjacentDirection[1] = AdjacentDirections[j, 1];
            adjacentDirection[2] = AdjacentDirections[j, 2];

            int adjacentNode = FindNode(pos + (adjacentDirection * nodesize));
            if (adjacentNode > 0 && adjacentNode != i && !obstacleNode[adjacentNode])
            {
                UnityEngine.Debug.DrawLine(pos, pos + adjacentDirection, Color.red, 50.0f);
                counter++;
            }

        }*/

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            currentNode--;
            NodePrint(currentNode);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            currentNode++;
            NodePrint(currentNode);
        }
    }

    void RenderNodes()
    {
        int renderedCount = 0;
        for (int i = 0; i < childsTotal.Length; i++)
        {
            if (childsTotal[i] == 0)
            {
                if (obstacleNode[i])
                {
                    renderedCount++;
                    RenderNode(i, Color.red);

                }
                
            }

            
        }
        DebugOutput.Shout(childsTotal.Length.ToString()+ " rendered count: "+renderedCount.ToString());

    }
    void NodePrint(int index)
    {
        DebugOutput.Shout( "Index: "+index.ToString() +
        " Parent: "+parent[index].ToString() +
        " total childs: " + childsTotal[index].ToString() +
        " Left: " + nodeToLeft[index].ToString() +
        " Right: " + nodeToRight[index].ToString() +
        " levelDepth: " + levelDepth[index].ToString() +
        " cornerId: " + cornerID[index].ToString() +
        " Binary Loc: "+Convert.ToString(binaryLocation[index,0],2)+", "+Convert.ToString(binaryLocation[index,1],2)+", "+Convert.ToString(binaryLocation[index,2],2) +
        " Binary Loc: "+binaryLocation[index,0].ToString()+", "+binaryLocation[index,1].ToString()+", "+binaryLocation[index,2].ToString()
            );
    }
    void RenderNode(int i,Color col)
    {
        Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
        float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0]))*size*2;
        pos = (pos / (float)Math.Pow(2, levelDepth[0]))*size;
       
        pos = ((pos * 2.0f) - Vector3.one * size )+transform.position;

        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[1] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[2] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[1] * nodesize, pos + corners[3] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[2] * nodesize, pos + corners[3] * nodesize, col, 50.0f);

        UnityEngine.Debug.DrawLine(pos + corners[4] * nodesize, pos + corners[5] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[4] * nodesize, pos + corners[6] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[5] * nodesize, pos + corners[7] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[6] * nodesize, pos + corners[7] * nodesize, col, 50.0f);

        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[4] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[1] * nodesize, pos + corners[5] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[2] * nodesize, pos + corners[6] * nodesize, col, 50.0f);
        UnityEngine.Debug.DrawLine(pos + corners[3] * nodesize, pos + corners[7] * nodesize, col, 50.0f);

        

    }
    int WorldToBinaryCoord(float f)
    {
        f = f - (transform.position.x);
        f = f + Vector3.one.x * size;
        f = f / 2.0f;
        f = f / size;
        f = f * (float)Math.Pow(2, levelDepth[0]);
        return (int)f;
    }

    int FindNode(Vector3 worldPoint)
    {
        worldPoint = worldPoint - (transform.position);
        worldPoint = worldPoint + Vector3.one * size;
        worldPoint = worldPoint / 2.0f;
        worldPoint = worldPoint / size;
        worldPoint = worldPoint * (float)Math.Pow(2, levelDepth[0]);
        worldPoint.x = (int)worldPoint.x;
        worldPoint.y = (int)worldPoint.y;
        worldPoint.z = (int)worldPoint.z;
        
        if(worldPoint.x <= 0 || worldPoint.x > (float)Math.Pow(2, levelDepth[0]) ||
            worldPoint.y <= 0 || worldPoint.y > (float)Math.Pow(2, levelDepth[0]) ||
            worldPoint.z <= 0 || worldPoint.z > (float)Math.Pow(2, levelDepth[0])
            )
        {
            return -1;
        }

        //return TraverseToNode(0, (int)worldPoint.x, (int)worldPoint.y, (int)worldPoint.z);

        int banan = TraverseToNode(0, (int)worldPoint.x, (int)worldPoint.y, (int)worldPoint.z);
        
        //RenderNode(banan,Color.blue);
        return banan;
    }

    int TraverseToNode(int index, int targetX, int targetY, int TargetZ)
    {
        while (childsTotal[index] > 0)
        {
            int childBranchBit = 1 << (levelDepth[index]-1);

            int childIndex =
            (((targetX & childBranchBit) >> levelDepth[index] - 1)) +
            (((targetY & childBranchBit) >> levelDepth[index] - 1) * 4) +
            (((TargetZ & childBranchBit) >> levelDepth[index] - 1) * 2);

            index = index+1;
            while (childIndex > 0)
            {
                index = nodeToRight[index];
                childIndex--;
            }
        }
        return index;
    }
   
    void FindNeighBors(int sourceindex)
    {

        int binaryCellSize = 1 << levelDepth[sourceindex];

       // NodePrint(sourceindex);
        int commonAncestor = parent[sourceindex];
        for (int i = 0; i < 26; i++)
        {
            target[i,0] = binaryLocation[sourceindex, 0];
            target[i,1] = binaryLocation[sourceindex, 1];
            target[i,2] = binaryLocation[sourceindex, 2];

#region GetNeighborCoords

            for (int j = 0; j < 3; j++)
            {

                //verifying that the neighbor is actually inside the bounds(exist)

                if ((binaryLocation[sourceindex, j] + binaryCellSize) * AdjacentDirections[i, j] < (1 << (levelDepth[0] + 1)) &&
                    (binaryLocation[sourceindex, j] + (AdjacentDirections[i, j] * (1 << levelDepth[sourceindex]))) > 0)
                {
                    if (AdjacentDirections[i, j] == -1)
                    {
                        target[i, j] = binaryLocation[sourceindex, j] -1;
                    }
                    else
                    {
                        target[i,j] += AdjacentDirections[i, j]* (1 << levelDepth[sourceindex]);
                    }
                }
            }
#endregion
            //check if neighbor has the same parent, may as well do some work while we can.

            if ((binaryLocation[commonAncestor, 0] & target[i, 0]) == binaryLocation[commonAncestor, 0] &&
                (binaryLocation[commonAncestor, 1] & target[i, 1]) == binaryLocation[commonAncestor, 1] &&
                (binaryLocation[commonAncestor, 2] & target[i, 2]) == binaryLocation[commonAncestor, 2])
            {
                TraverseToNeighbors(sourceindex,commonAncestor, i);

            }
            else
            {
                TraverseToNeighbors(sourceindex, 0, i);
            }


        }      

    }
    void TraverseToNeighbors(int sourceIndex,int index,int targetID)
    {
        //check if neighbor has been found already
    }
    void LocateNeighbor(int sourceindex,int direction)
    {

        int binaryCellSize = 1 << levelDepth[sourceindex];

        int[] target = new int[3];
        target[0] = binaryLocation[sourceindex, 0];
        target[1] = binaryLocation[sourceindex, 1];
        target[2] = binaryLocation[sourceindex, 2];

        bool passedCheckup = true;
        for (int i = 0; i < 3 ; i++)
        {
            //verifying that the neighbor is actually inside the bounds(exist)
            if((binaryLocation[sourceindex, i] + binaryCellSize) * AdjacentDirections[direction, i] < (1 << (levelDepth[0] + 1)) &&
                (binaryLocation[sourceindex, i] + (1 << levelDepth[0]) * AdjacentDirections[direction, i]) > 0) 
            {
                if(AdjacentDirections[direction,i] == -1)
                {
                    target[i] = binaryLocation[sourceindex, i] - 0x00000001;
                }
                {
                    target[i] += binaryCellSize*AdjacentDirections[direction,i];
                }
            }
            else
            {
                passedCheckup = false;
                break;
            }
        }
        if(passedCheckup)
        {   //establishing Common ancestor, there should be a better/faster way of doing this.
            int commonAncestor = parent[sourceindex];
            while (commonAncestor > 0 && (((binaryLocation[sourceindex, 0] ^ target[0]) & (1 << levelDepth[commonAncestor])) > 0
                ||(int)((binaryLocation[sourceindex, 1] ^ target[1]) & (1 << levelDepth[commonAncestor])) > 0
                ||((binaryLocation[sourceindex, 2] ^ target[2]) & (1 << levelDepth[commonAncestor])) > 0))
            {
                commonAncestor = parent[commonAncestor];
            }
            TraverseToNode(commonAncestor, target[0], target[1], target[2]);

     
            RenderNode(TraverseToNode(commonAncestor, target[0], target[1], target[2]), Color.cyan);
        }
 
    }

}
