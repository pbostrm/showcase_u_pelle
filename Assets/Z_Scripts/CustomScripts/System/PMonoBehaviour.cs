﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class PMonoBehaviour : MonoBehaviour
{

    public Transform GetSetTransform(string name)
    {
        GameObject g = GameObject.Find(name);
        if (g == null)
        {
            g = new GameObject(name);
        }
        Transform t = g.transform;
        return t;
    }
    public Transform GetSetTransform(string name,Transform setParent)
    {
        GameObject g = GameObject.Find(name);
        if (g == null)
        {
            g = new GameObject(name);
        }
        Transform t = g.transform;
        t.parent = setParent;
        return t;
    }
}