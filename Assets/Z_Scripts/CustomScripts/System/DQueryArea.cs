﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;

public class DQueryArea 
{
    int[] nodes;
    public DQueryArea()
    {
    }
    static public void BuildAndSaveSpawnArea(string fileName,DOctree source,List<DQueryLocation> locations,Vector3 position)
    {
        int nodecount = 0;
        int foundnode = 0;

        List<int> nodeIndices = new List<int>();
        for (int i = 0; i < source.nodeCount;i++ )
        {
            if (source.childsTotal[i] == 0)
            {
                if (!source.obstacleNode[i])
                {

                    //time to check if it intersects
                    for (int j = 0; j < locations.Count;j++ )
                    {

                        if (source.IntersectNode(i, position+locations[j].position, locations[j].radius))
                        {
                            Debug.DrawLine(position + locations[j].position, position + locations[j].position + (Vector3.up * locations[j].radius), Color.red, 10.0f);
                            source.RenderNode(i,Color.cyan,10.0f);
                            foundnode = i;
                            nodecount++;

                            nodeIndices.Add(i);
                            break;
                        }
                    }

                }
            }
        }

        SaveAreaToFile(fileName,nodeIndices);
        Debug.Log("we found " + nodeIndices.Count.ToString() + " nodes");

    }
    static public void SaveAreaToFile(string filename, List<int> indices)
    {
        Debug.Log("Saving to file... "+filename);
		string fileName = "Assets/Resources/"+filename+".bytes";

        if (File.Exists(fileName))
		{
			//Console.WriteLine("{0} already exists!", FILE_NAME);
			File.Delete(fileName);
			//return;
		}
		using (FileStream fs = new FileStream(fileName, FileMode.CreateNew))
		{
			using (BinaryWriter w = new BinaryWriter(fs))
			{
                w.Write(indices.Count);
                for(int i = 0;i< indices.Count;i++)
                {
                    w.Write(indices[i]);
                }
			}
		}

    }
    static public DQueryArea LoadAreaFromFile(string fileName)
    {
        byte[] ta = null;

#if UNITY_EDITOR
        try
        {
            ta = Load("Assets/Resources/" + fileName);

        }
        catch (System.Exception ex)
        {
            UnityEngine.Debug.Log("Cant load file lol + " + ex);
        }
#endif

#if !UNITY_EDITOR
        ta = ((TextAsset)Resources.Load(fileName)).bytes;

#endif

        if (ta == null)
        {
            return null;
        }
        if (ta != null && ta.Length != null)
        {
            BinaryReader br = new BinaryReader(new MemoryStream(ta));

            DQueryArea area = new DQueryArea();

            area.nodes = new int[br.ReadInt32()];

            for (int i = 0; i < area.nodes.Length;i++ )
            {
                area.nodes[i] = br.ReadInt32();
            }

            return area;
        }
        
        return null;
    }

    static public byte[] Load(string fileName)
    {

        FileStream file = new FileStream(fileName + ".bytes", FileMode.Open, FileAccess.Read);

        byte[] bytes = new byte[file.Length];
        file.Read(bytes, 0, (int)file.Length);
        return bytes;
    }
    public int getRandomNode()
    {
        return nodes[UnityEngine.Random.Range(0, nodes.Length - 1)];
    }
}

[System.Serializable]
public class DQueryLocation
{
    public Vector3 position;
    public float radius;
}