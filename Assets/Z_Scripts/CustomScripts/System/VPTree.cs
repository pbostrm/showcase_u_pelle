﻿using System;
using System.Collections;
using UnityEngine;

public class VPTree
{

    public Vector3[] points;
    public int[] indices;
    int[] workIndices;
    public int[] invertedIndices;
    int count;

    public VPTree(int length)
    {
        points = new Vector3[length];
        indices = new int[length];
        invertedIndices = new int[length];
        workIndices = new int[length];

        for (int i = 0; i < points.Length; i++)
        {
            workIndices[i] = i;
        }

        count = points.Length;
        //HeapFullTest();
        
    }
    public void PushTree(Vector3[] input)
    {
        for (int i = 0; i < count; i++)
        {
            points[i] = input[i];
            indices[i] = workIndices[i];
            invertedIndices[indices[i]] = i;
        }
    }
    public void SyncTree()
    {
        for (int i = 0; i < count; i++)
        {
            indices[i] = workIndices[i];
            invertedIndices[indices[i]] = i;
        }
    }
    public void find2(int index,int findRange)
    {
        int indiceIndex = invertedIndices[index];

        if (indiceIndex - findRange / 2 < 0)
        {
            indiceIndex = 1 + findRange / 2;
        }
        for(int i = indiceIndex-findRange/2;(i< invertedIndices.Length)&&(i< indiceIndex+findRange/2);i++)
        {
            if ((points[index] - points[indices[i]]).sqrMagnitude < 20)
            {
                //do something;
            }
            //DebugOutput.Shout(i.ToString());
            Debug.DrawLine(points[indices[i - 1]], points[indices[i]], Color.cyan);
        }
    }
    public void find(int index, int findRange)
    {
        int indiceIndex = invertedIndices[index];

        if (indiceIndex - findRange / 2 < 0)
        {
            indiceIndex = 1 + findRange / 2;
        }
        for (int i = indiceIndex - findRange / 2; (i < invertedIndices.Length) && (i < indiceIndex + findRange / 2); i++)
        {
            if ((points[index] - points[indices[i]]).sqrMagnitude < 20)
            {
                //do something;
            }
        }
    }
    public void HeapFullTest()
    {

        int heapsize = count;

        int offset = 0;
        int counter = 0;
        while ((heapsize % 2) != 1)
        {
            for (int i = 0; i < workIndices.Length / heapsize; i++)
            {

                offset = (i) * heapsize;
                counter++;
                HeapSort(points[workIndices[offset]], offset, heapsize);
            }
            heapsize = heapsize / 2;
        }
      /*  for (int i = 0; i < indices.Length; i++)
        {
            invertedIndices[indices[i]] = i;
        }*/
    }


    void HeapSort(Vector3 point, int nodeIndex, int length)
    {
        
        //int heapsize = length;
        for (int p = (length - 1) / 2; p >= 0; p--)
        {
            MaxHeapify(point, nodeIndex, length, p);
            
        }

       for (int i = nodeIndex + length - 1; i > nodeIndex; i--)
        {
            swapIndex(i, nodeIndex);
           // heapsize--;
            length--;
            MaxHeapify(point,nodeIndex, length, 0);
        }
        
    }
    void MaxHeapify(Vector3 point,int nodeIndex, int heapsize,int index)
    {
        int left = ((index + 1) * 2 - 1);
        int right = ((index + 1) * 2);
        int shortest = 0;

        if (left < heapsize && (point - points[workIndices[nodeIndex + left]]).sqrMagnitude
            > (point - points[workIndices[nodeIndex + index]]).sqrMagnitude)
        {
            shortest = left;
        }
        else
        {
            shortest = index;
        }

        if (right < heapsize && (point - points[workIndices[nodeIndex + right]]).sqrMagnitude
            > (point - points[workIndices[nodeIndex + shortest]]).sqrMagnitude)
        {
            shortest = right;
        }

        if (shortest != index)
        {
            swapIndex(nodeIndex + index, nodeIndex + shortest);
            MaxHeapify(point, nodeIndex, heapsize, shortest);
        }

    }

    int swapSpace;
    void swapIndex(int a,int b)
    {
        swapSpace = workIndices[a];
        workIndices[a] = workIndices[b];
        workIndices[b] = swapSpace;
    }

    void DrawAtPoint(int pointIndex,Color col)
    {
       // DebugOutput.Shout(points[indices[pointIndex]] + " index " + pointIndex.ToString());

        Debug.DrawLine(points[indices[pointIndex]]  - Vector3.one,
            points[indices[pointIndex]]  + Vector3.one, col, 300.0f);
        Debug.DrawLine(points[indices[pointIndex]] + (Vector3.left + Vector3.up),
            points[indices[pointIndex]]  + Vector3.right+Vector3.down, col, 300.0f);
    }   

    // old stuff

    void MedianHeapTest()
    {

        int heapsize = count;

        int offset = 0;
        while ((heapsize % 2) != 1)
        {
            for (int i = 0; i < indices.Length / heapsize; i++)
            {
                offset = (i) * heapsize;
                for (int p = (heapsize - 1) / 2; p >= 0; p--)
                {
                    MedianHeapify(points[indices[offset]], offset, heapsize, p);
                }

                for (int j = heapsize - 1; j > 0; j--)
                {
                    swapIndex(j, offset);
                    MaxHeapify(points[indices[offset]], offset, j - 1, 0);
                }

                // DebugOutput.Shout(heapsize.ToString());
            }
            heapsize = heapsize / 2;
        }
        //DebugOutput.Shout("heapsize: " + heapsize.ToString());
        /*
        heapsize = count;

        for (int p = (heapsize - 1) / 2; p >= 0; p--)
        {
            MedianHeapify(points[indices[0]], 0, indices.Length, p);

        }
        heapsize = count/2;
        for (int p = (heapsize - 1) / 2; p >= 0; p--)
        {
            MedianHeapify(points[indices[heapsize]], heapsize, heapsize, p);

        }
        */
        DrawAtPoint(0, Color.yellow);
        string order = "the sorted order is: " + indices[0].ToString() + " ";
        for (int i = 1; i < indices.Length; i++)
        {
            /*
                        if (i < (count / 2))
                        {
                            //DrawAtPoint(i, Color.green);
                        }
                        else 
                        {
                            //DrawAtPoint(i, Color.red);
                        }*/
            Debug.DrawLine(points[indices[i - 1]], points[indices[i]], Color.blue, 200.0f);
            //    DebugOutput.Shout((points[indices[0]] - points[indices[i]]).sqrMagnitude.ToString() +" "+ indices[i].ToString());
            order = order + indices[i].ToString() + " ";
        }
        DebugOutput.Shout(order);

    }
    void MedianHeapify(Vector3 point, int nodeIndex, int heapsize, int index)
    {
        int left = (((index + 1) * 2) - 1);
        int right = ((index + 1) * 2);
        int shortest = 0;

        if (left < heapsize && (point - points[indices[nodeIndex + left]]).sqrMagnitude
            < (point - points[indices[nodeIndex + index]]).sqrMagnitude)
        {
            shortest = left;
        }
        else
        {
            shortest = index;
        }

        try
        {
            if (right < heapsize && (point - points[indices[nodeIndex + right]]).sqrMagnitude
            < (point - points[indices[nodeIndex + shortest]]).sqrMagnitude)
            {
                shortest = right;
            }
        }
        catch (System.Exception ex)
        {
            DebugOutput.Shout(nodeIndex.ToString() + " " + right.ToString() + " " + shortest.ToString());
        }


        if (shortest != index)
        {
            swapIndex(nodeIndex + index, nodeIndex + shortest);
            MedianHeapify(point, nodeIndex, heapsize, shortest);
        }
    }
    void HeapSortTest()
    {
        HeapSort(points[indices[0]], 0, indices.Length);
        DrawAtPoint(0, Color.yellow);

        string order = "the sorted order is: " + indices[0].ToString() + " "; ;
        for (int i = 1; i < 10; i++)
        {
            Debug.DrawLine(points[indices[i - 1]], points[indices[i]], Color.blue, 200.0f);
            DebugOutput.Shout((points[indices[0]] - points[indices[i]]).sqrMagnitude.ToString());
            order = order + indices[i].ToString() + " ";
            //DebugOutput.Shout("Rendering line from " + indices[i - 1].ToString() + " to " + indices[i].ToString());


        }
        DebugOutput.Shout(order);
        HeapSort(points[indices[10]], 10, indices.Length - 10);
        DrawAtPoint(10, Color.magenta);
        order = "the sorted order is: " + indices[10].ToString() + " ";
        for (int i = 11; i < indices.Length; i++)
        {
            Debug.DrawLine(points[indices[i - 1]], points[indices[i]], Color.red, 200.0f);
            order = order + indices[i].ToString() + " ";
            //    DebugOutput.Shout((points[indices[10]] - points[indices[i]]).sqrMagnitude.ToString());
        }
        DebugOutput.Shout(order);
    }
    float SqrDistanceFromNode(int a, int b)
    {
        return (points[indices[a]] - points[indices[a + b]]).sqrMagnitude;
    }
    float SqrDistance(int a, int b)
    {
        return (points[indices[a]] - points[indices[b]]).sqrMagnitude;
    }
    public void find3(int index, float distance)
    {
        int indiceIndex = invertedIndices[index];
        /*if (index % 2 == 1)
        {
            if (index < indices.Length - 1)
            {
                Debug.DrawLine(points[index], points[indices[indiceIndex + 1]], Color.blue, 200.0f);
            }
        }
        else
        {
            Debug.DrawLine(points[index], points[indices[indiceIndex - 1]], Color.blue, 200.0f);
        }
         */
        DrawAtPoint(indiceIndex, Color.red);
        int parentIndex;
        if (indiceIndex > 0)
        {

            /*int left = (((index + 1) * 2) - 1);
            int right = ((index + 1) * 2);
            */
            // parentIndex = ((indiceIndex + (indiceIndex % 2)) / 2) - 1;
            parentIndex = ((indiceIndex / 2));
        }
        else
        {
            parentIndex = indiceIndex;
        }

        int left = (((parentIndex + 1) * 2) - 1);
        int right = ((parentIndex + 1) * 2);
        // Debug.DrawLine(points[index], points[indices[parentIndex]], Color.green, 200.0f);
        Debug.DrawLine(points[indices[parentIndex]], points[indices[left]], Color.cyan, 200.0f);
        Debug.DrawLine(points[indices[parentIndex]], points[indices[right]], Color.magenta, 200.0f);
        if (indiceIndex % 2 == 1)
        {
            if (indiceIndex < indices.Length - 1)
            {
                Debug.DrawLine(points[indiceIndex], points[indices[indiceIndex + 1]], Color.blue, 200.0f);
            }
        }
        else if (indiceIndex > 0)
        {
            Debug.DrawLine(points[indiceIndex], points[indices[indiceIndex - 1]], Color.blue, 200.0f);
        }

    }
}
