﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Diagnostics;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DOctree
{
    int[] nodeToRight;
    int[] nodeToLeft;

    public int[] childsTotal;
    public bool[] obstacleNode;
    bool[] borderNode;
    Vector3[] borderUnDirection;
    float[,] obstacleOutDirection;
    int[] parent;
    int[] levelDepth;
    int[,] binaryLocation;
    int[] cornerID;
    public string BoundsName;
    public bool loadFromFile;
    public float size;
    int maxLevelDepth;
    public int nodeCount;

    int[,] target; //used for finding neighbors;

    int currentNode = 7768;

    static public  DOPtests bounds;
    public bool StressTest = false;
    //Borders! Borders contain exit vector(calculated).

    #region properties
    public static readonly short[,] AdjacentDirections = new short[26, 3]
	{
		{1,0,0},
		{-1,0,0},

		{1,1,0},
		{-1,-1,0},
		{-1,1,0},
		{1,-1,0},

		{1,0,1},
		{-1,0,-1},
		{1,0,-1},
		{-1,0,1},

		{1,1,1},
		{-1,1,1},
		{-1,-1,1},
		{-1,-1,-1},
		{1,-1,-1},
		{1,1,-1},
		{1,-1,1},
		{-1,1,-1},
		
		{0,1,1},
		{0,-1,-1},
		{0,-1,1},
		{0,1,-1},

		{0,0,1},
		{0,0,-1},

		{0,1,0},
		{0,-1,0}
	};

    #endregion
    Vector3[] corners;

    public Vector3 position;
    public DOctree(string boundsName,Vector3 p)
    {
        position = p;
        corners = new Vector3[8];
        corners[0] = new Vector3(0f, 0f, 0f);
        corners[1] = new Vector3(1f, 0f, 0f);
        corners[2] = new Vector3(0f, 0f, 1f);
        corners[3] = new Vector3(1f, 0f, 1f);
        corners[4] = new Vector3(0f, 1f, 0f);
        corners[5] = new Vector3(1f, 1f, 0f);
        corners[6] = new Vector3(0f, 1f, 1f);
        corners[7] = new Vector3(1f, 1f, 1f);


        if (boundsName != null)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //----
            LoadBoundsFromFile("OctreeData/DOP_" + boundsName);
            BuildData();

            //---
            watch.Stop();

            DebugOutput.Shout("loading bounds DOPmode took " +
                watch.ElapsedMilliseconds.ToString() + " milliseconds to process");

            //RenderNodes();
      //      NodePrint(0);

    //        NodePrint(5457);
  //          FindNeighBors(5457);
          //  RenderNode(FindNode(new Vector3(25.035f, 25.880f, -23.500f)),Color.blue);

            #region stressTest

            if (StressTest)
            {
                watch.Reset();
                watch.Start();
                int neighborCheckCnt = 0;
                for (int j = 0; j < 1; j++)
                {
                    for (int i = 0; i < nodeCount; i++)
                    {
                        if (childsTotal[i] == 0)
                        {
                            neighborCheckCnt++;
                            FindNeighBors(i);
                        }
                    }
                }
                watch.Stop();

                DebugOutput.Shout("getting " + neighborCheckCnt.ToString() + " took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process");

                watch.Reset();
                watch.Start();
                Vector3 randomPoint = new Vector3();

                for (int j = 0; j < 100000; j++)
                {
                    randomPoint.x = UnityEngine.Random.Range(-40.0f, 40.0f);
                    randomPoint.y = UnityEngine.Random.Range(-40.0f, 40.0f);
                    randomPoint.z = UnityEngine.Random.Range(-40.0f, 40.0f);
                    FindNode(randomPoint);
                }
                watch.Stop();

                DebugOutput.Shout("finding random point took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process");

                watch.Reset();
                watch.Start();

                int[] neighborDirection = new int[3];

                int neighborCount = 0;
                Vector3 outDirection = new Vector3();
                Vector3 inDirection = new Vector3();

                int pasted = 3;
                Vector3 adjacentDirection = new Vector3();

                int counter = 0;

                
                for (int i = 1; i < nodeCount; i++)
                {
                    neighborCount = 0;
                    if (childsTotal[i] == 0 && obstacleNode[i])
                    {
                        for (int j = 0; j < 26; j++)
                        {
                            int[] target = new int[3];
                            target[0] = binaryLocation[i, 0];
                            target[1] = binaryLocation[i, 1];
                            target[2] = binaryLocation[i, 2];



                            for (int k = 0;k < 3; k++)
                            {

                                //verifying that the neighbor is actually inside the bounds(exist)
                               /* int binaryCellSize = 1 << levelDepth[i];

                                if ((binaryLocation[i, k] + binaryCellSize) * AdjacentDirections[j, k] < (1 << (levelDepth[0] + 1)) &&
                                    (binaryLocation[i, k] + (AdjacentDirections[j, k] * (1 << levelDepth[i]))) > 0)
                                {
                                    if (AdjacentDirections[j, k] == -1)
                                    {
                                        target[k] = binaryLocation[i, k] -1;
                                    }
                                    else
                                    {
                                        target[k] += AdjacentDirections[j, k]* (1 << levelDepth[i]);
                                    }
                                }

                                int adjacentNode = TraverseToNode(0, target[0], target[1], target[0]);
                                adjacentDirection[0] = AdjacentDirections[j, 0];
                                adjacentDirection[1] = AdjacentDirections[j, 1];
                                adjacentDirection[2] = AdjacentDirections[j, 2];

                                if (adjacentNode > 0 && adjacentNode != i && !obstacleNode[adjacentNode])
                                {
                                    Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
                                    float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
                                    pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

                                    pos = ((pos * 2.0f) - Vector3.one * size) + position;
                                    pos = pos + (Vector3.one * 0.5f) * nodesize;

                                    UnityEngine.Debug.DrawLine(pos, pos + adjacentDirection, Color.red, 50.0f);
                                    counter++;
                                }*/
                                 Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
                                float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
                                pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

                                pos = ((pos * 2.0f) - Vector3.one * size) + position;
                                pos = pos + (Vector3.one * 0.5f) * nodesize;
                                adjacentDirection[0] = AdjacentDirections[j, 0];
                                adjacentDirection[1] = AdjacentDirections[j, 1];
                                adjacentDirection[2] = AdjacentDirections[j, 2];

                                int adjacentNode = FindNode(pos + (adjacentDirection * nodesize));

                               // TraverseToNode(0,)
                                if (adjacentNode > 0 && adjacentNode != i && !obstacleNode[adjacentNode])
                                {
                                    UnityEngine.Debug.DrawLine(pos, pos + adjacentDirection, Color.red, 50.0f);
                                    counter++;
                                }
                            
                            }

                            
                           
                        }
                    }
                }

                watch.Stop();

                DebugOutput.Shout("bruteforce took  " +
                    watch.ElapsedMilliseconds.ToString() + " milliseconds to process " + counter.ToString());
            #endregion

            }    
        }
    }

    void LoadBoundsFromFile(string fileName)
    {
        byte[] ta = null;

#if UNITY_EDITOR

        //ta = (TextAsset)AssetDatabase.LoadAssetAtPath("Assets/Textures/texture.jpg", typeof(TextAsset));
        try
        {
            ta = Load("Assets/Resources/"+fileName);

        }
        catch (System.Exception ex)
        {
            UnityEngine.Debug.Log("Cant load file lol + "+ex);
        }
#endif

#if !UNITY_EDITOR
        ta = ((TextAsset)Resources.Load(fileName)).bytes;

#endif 

        if (ta == null)
        {
            return;
        }
        if (ta != null  && ta.Length != null)
        {
            BinaryReader br = new BinaryReader(new MemoryStream(ta));
            position = new Vector3();
            position.x = (float)br.ReadDouble();
            position.y = (float)br.ReadDouble();
            position.z = (float)br.ReadDouble();
            size = (float)br.ReadDouble();
            maxLevelDepth = br.ReadInt32();
            nodeCount = br.ReadInt32();
            DebugOutput.Shout("There is "+nodeCount+" nodes in the file, maxLevelDepth= "+maxLevelDepth.ToString()+" size: "+size.ToString());

            childsTotal = new int[nodeCount];
            nodeToRight = new int[nodeCount];
            nodeToLeft = new int[nodeCount];
            obstacleNode = new bool[nodeCount];

            for (int i = 0; i < nodeCount;i++ )
            {
                childsTotal[i] = br.ReadInt32()-1;
                nodeToRight[i] = i+childsTotal[i]+1;
                obstacleNode[i] = br.ReadBoolean();
            }
        }
    }

    private byte[] Load(string fileName)
    {
        //MemoryStream ms = new MemoryStream();
        FileStream file = new FileStream(fileName+".bytes", FileMode.Open, FileAccess.Read);

        byte[] bytes = new byte[file.Length];
        file.Read(bytes, 0, (int)file.Length);
        return bytes;
       // ms.Write(bytes, 0, (int)file.Length);


    }
    void BuildData()
    {
        parent = new int[nodeCount];
        levelDepth = new int[nodeCount];
        binaryLocation = new int[nodeCount, 3];
        cornerID = new int[nodeCount];

        levelDepth[0] = maxLevelDepth;

        //making leveldepth and parent hierarchy
        for (int i = 0; i < nodeCount; i++)
        {
            for (int j = i + 1; j < nodeToRight[i]; j++)
            {
                parent[j] = i;
                levelDepth[j] = levelDepth[i] - 1;
            }

        }
        //creating left and right pointers
        for (int i = 1; i < nodeCount; i++)
        {
            if (nodeToRight[i] < nodeCount && nodeToRight[parent[i]] != nodeToRight[i])
            {
                nodeToLeft[nodeToRight[i]] = i;
            }

        }

        //Building corners and binary locations
        for (int i = 0; i < nodeCount; i++)
        {
            if (nodeToLeft[i] == 0)
            {
                cornerID[i] = 0;
            }
            else
            {
                cornerID[i] = cornerID[nodeToLeft[i]] + 1;
            }
            if (i > 0)
            {
                binaryLocation[i, 0] = binaryLocation[parent[i], 0];
                binaryLocation[i, 1] = binaryLocation[parent[i], 1];
                binaryLocation[i, 2] = binaryLocation[parent[i], 2];


                if (cornerID[i] % 2 != 0)
                {
                    binaryLocation[i, 0] = binaryLocation[parent[i], 0] + (int)Math.Pow(2, levelDepth[i]);
                }
                if (cornerID[i] > 3)
                {
                    binaryLocation[i, 1] = binaryLocation[parent[i], 1] + (int)Math.Pow(2, levelDepth[i]);

                }
                if (cornerID[i] == 2 || cornerID[i] == 3 || cornerID[i] == 6 || cornerID[i] == 7)
                {
                    binaryLocation[i, 2] = binaryLocation[parent[i], 2] + (int)Math.Pow(2, levelDepth[i]);

                }
            }
        }
        target = new int[26, 3];

        borderNode = new bool[nodeCount];
        borderUnDirection = new Vector3[nodeCount];

        for (int i = 1; i < nodeCount; i++)
        {
            if (childsTotal[i] == 0)
            {

            }

        }
        obstacleOutDirection = new float[nodeCount,6];
    }

    public int checkObstacle(Vector3 point)
    {
        int cnode = FindNode(point);

        if (cnode > 0 && obstacleNode[cnode])
        {
            return cnode;
        }
        return -1;

    }
    public void findClosestEscape(int pointindex,ref Vector3 dir)
    {
        Vector3 closestAdjacentDir = new Vector3();
        float minDir = 500000000000000.0f;

        Vector3 adjacentDirection = new Vector3();
        for (int j = 0; j < 26; j++)
        {
            Vector3 pos = new Vector3(binaryLocation[pointindex, 0], binaryLocation[pointindex, 1], binaryLocation[pointindex, 2]);
            float nodesize = (float)(Math.Pow(2, levelDepth[pointindex]) / Math.Pow(2, levelDepth[0])) * size * 2;
            pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

            pos = ((pos * 2.0f) - Vector3.one * size) + position;
            pos = pos + (Vector3.one * 0.5f) * nodesize;
            adjacentDirection[0] = AdjacentDirections[j, 0];
            adjacentDirection[1] = AdjacentDirections[j, 1];
            adjacentDirection[2] = AdjacentDirections[j, 2];

            int adjacentNode = FindNode(pos + (adjacentDirection * nodesize));

            // TraverseToNode(0,)
            if (adjacentNode > 0 && adjacentNode != pointindex && !obstacleNode[adjacentNode])
            {
                float disSqr = (dir - adjacentDirection).sqrMagnitude;
                if ( disSqr <= minDir)
                {
                    minDir = disSqr;
                    closestAdjacentDir = adjacentDirection;
                }
                //UnityEngine.Debug.DrawLine(pos, pos + adjacentDirection, Color.red, 50.0f);
               // counter++;
            }

        }
        dir = closestAdjacentDir;
    }

    void RenderNodes()
    {
        //int renderedCount = 0;
        for (int i = 0; i < childsTotal.Length; i++)
        {
            if (childsTotal[i] == 0)
            {
                if (obstacleNode[i])
                {
                   // renderedCount++;
                    RenderNode(i, Color.red);

                }
                
            }            
        }
       // DebugOutput.Shout(childsTotal.Length.ToString()+ " rendered count: "+renderedCount.ToString());

    }
    void NodePrint(int index)
    {
        DebugOutput.Shout( "Index: "+index.ToString() +
        " Parent: "+parent[index].ToString() +
        " total childs: " + childsTotal[index].ToString() +
        " Left: " + nodeToLeft[index].ToString() +
        " Right: " + nodeToRight[index].ToString() +
        " levelDepth: " + levelDepth[index].ToString() +
        " cornerId: " + cornerID[index].ToString() +
        " Binary Loc: "+Convert.ToString(binaryLocation[index,0],2)+", "+Convert.ToString(binaryLocation[index,1],2)+", "+Convert.ToString(binaryLocation[index,2],2) +
        " Binary Loc: "+binaryLocation[index,0].ToString()+", "+binaryLocation[index,1].ToString()+", "+binaryLocation[index,2].ToString()
            );
    }
    public void RenderNode(int i,Color col)
    {
        if (i >= 0 && i < nodeCount)
        {
            RenderNode(i, col, 50.0f);
        }
    }
    public void RenderNode(int i, Color col, float t)
    {
        Vector3 pos = new Vector3(binaryLocation[i, 0], binaryLocation[i, 1], binaryLocation[i, 2]);
        float nodesize = (float)(Math.Pow(2, levelDepth[i]) / Math.Pow(2, levelDepth[0])) * size * 2;
        pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

        pos = ((pos * 2.0f) - Vector3.one * size) + position;

        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[1] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[2] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[1] * nodesize, pos + corners[3] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[2] * nodesize, pos + corners[3] * nodesize, col, t);

        UnityEngine.Debug.DrawLine(pos + corners[4] * nodesize, pos + corners[5] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[4] * nodesize, pos + corners[6] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[5] * nodesize, pos + corners[7] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[6] * nodesize, pos + corners[7] * nodesize, col, t);

        UnityEngine.Debug.DrawLine(pos + corners[0] * nodesize, pos + corners[4] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[1] * nodesize, pos + corners[5] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[2] * nodesize, pos + corners[6] * nodesize, col, t);
        UnityEngine.Debug.DrawLine(pos + corners[3] * nodesize, pos + corners[7] * nodesize, col, t);
    }
    int WorldToBinaryCoord(float f)
    {
        f = f - (position.x);
        f = f + Vector3.one.x * size;
        f = f / 2.0f;
        f = f / size;
        f = f * (float)Math.Pow(2, levelDepth[0]);
        return (int)f;
    }
    public Vector3 GetWorldCoord(int index)
    {
        Vector3 pos = new Vector3(binaryLocation[index, 0], binaryLocation[index, 1], binaryLocation[index, 2]);
        float nodesize = (float)(Math.Pow(2, levelDepth[index]) / Math.Pow(2, levelDepth[0])) * size * 2;
        pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

        pos = ((pos * 2.0f) - Vector3.one * size) + position;
        pos = pos + (Vector3.one * 0.5f) * nodesize;

        return pos;
    }
    int FindNode(Vector3 worldPoint)
    {
        worldPoint = worldPoint - (position);
        worldPoint = worldPoint + Vector3.one * size;
        worldPoint = worldPoint / 2.0f;
        worldPoint = worldPoint / size;
        worldPoint = worldPoint * (float)Math.Pow(2, levelDepth[0]);
        worldPoint.x = (int)worldPoint.x;
        worldPoint.y = (int)worldPoint.y;
        worldPoint.z = (int)worldPoint.z;
        
        if(worldPoint.x <= 0 || worldPoint.x > (float)Math.Pow(2, levelDepth[0]) ||
            worldPoint.y <= 0 || worldPoint.y > (float)Math.Pow(2, levelDepth[0]) ||
            worldPoint.z <= 0 || worldPoint.z > (float)Math.Pow(2, levelDepth[0])
            )
        {
            return -1;
        }

        //return TraverseToNode(0, (int)worldPoint.x, (int)worldPoint.y, (int)worldPoint.z);

        int banan = TraverseToNode(0, (int)worldPoint.x, (int)worldPoint.y, (int)worldPoint.z);
        
        //RenderNode(banan,Color.blue);
        return banan;
    }
    public bool IntersectNode(int index,Vector3 point, float radius)
    {
        //Vector3 pos = new Vector3(binaryLocation[index, 0], binaryLocation[index, 1], binaryLocation[index, 2]);
        float nodeRadius = (float)(Math.Pow(2, levelDepth[index]) / Math.Pow(2, levelDepth[0])) * size;
        //pos = (pos / (float)Math.Pow(2, levelDepth[0])) * size;

        //pos = ((pos * 2.0f) - Vector3.one * size);

        Vector3 pos = GetWorldCoord(index);

        for (int i = 0; i < 3; i++)
        {
            if (pos[i] + nodeRadius < point[i] - radius || point[i] + radius < pos[i] - nodeRadius)
            {
                return false;
            }
        }

        return true;
        
    }
    int TraverseToNode(int index, int targetX, int targetY, int TargetZ)
    {
        while (childsTotal[index] > 0)
        {
            int childBranchBit = 1 << (levelDepth[index]-1);

            int childIndex =
            (((targetX & childBranchBit) >> levelDepth[index] - 1)) +
            (((targetY & childBranchBit) >> levelDepth[index] - 1) * 4) +
            (((TargetZ & childBranchBit) >> levelDepth[index] - 1) * 2);

            index = index+1;
            while (childIndex > 0)
            {
                index = nodeToRight[index];
                childIndex--;
            }
        }
        return index;
    }
   
    void FindNeighBors(int sourceindex)
    {

        int binaryCellSize = 1 << levelDepth[sourceindex];

       // NodePrint(sourceindex);
        int commonAncestor = parent[sourceindex];
        for (int i = 0; i < 26; i++)
        {
            target[i,0] = binaryLocation[sourceindex, 0];
            target[i,1] = binaryLocation[sourceindex, 1];
            target[i,2] = binaryLocation[sourceindex, 2];

#region GetNeighborCoords

            for (int j = 0; j < 3; j++)
            {

                //verifying that the neighbor is actually inside the bounds(exist)

                if ((binaryLocation[sourceindex, j] + binaryCellSize) * AdjacentDirections[i, j] < (1 << (levelDepth[0] + 1)) &&
                    (binaryLocation[sourceindex, j] + (AdjacentDirections[i, j] * (1 << levelDepth[sourceindex]))) > 0)
                {
                    if (AdjacentDirections[i, j] == -1)
                    {
                        target[i, j] = binaryLocation[sourceindex, j] -1;
                    }
                    else
                    {
                        target[i,j] += AdjacentDirections[i, j]* (1 << levelDepth[sourceindex]);
                    }
                }
            }
#endregion
            //check if neighbor has the same parent, may as well do some work while we can.

            if ((binaryLocation[commonAncestor, 0] & target[i, 0]) == binaryLocation[commonAncestor, 0] &&
                (binaryLocation[commonAncestor, 1] & target[i, 1]) == binaryLocation[commonAncestor, 1] &&
                (binaryLocation[commonAncestor, 2] & target[i, 2]) == binaryLocation[commonAncestor, 2])
            {
                TraverseToNeighbors(sourceindex,commonAncestor, i);

            }
            else
            {
                TraverseToNeighbors(sourceindex, 0, i);
            }


        }      

    }
    void TraverseToNeighbors(int sourceIndex,int index,int targetID)
    {
        //check if neighbor has been found already
    }
    void LocateNeighbor(int sourceindex,int direction)
    {

        int binaryCellSize = 1 << levelDepth[sourceindex];

        int[] target = new int[3];
        target[0] = binaryLocation[sourceindex, 0];
        target[1] = binaryLocation[sourceindex, 1];
        target[2] = binaryLocation[sourceindex, 2];

        bool passedCheckup = true;
        for (int i = 0; i < 3 ; i++)
        {
            //verifying that the neighbor is actually inside the bounds(exist)
            if((binaryLocation[sourceindex, i] + binaryCellSize) * AdjacentDirections[direction, i] < (1 << (levelDepth[0] + 1)) &&
                (binaryLocation[sourceindex, i] + (1 << levelDepth[0]) * AdjacentDirections[direction, i]) > 0) 
            {
                if(AdjacentDirections[direction,i] == -1)
                {
                    target[i] = binaryLocation[sourceindex, i] - 0x00000001;
                }
                {
                    target[i] += binaryCellSize*AdjacentDirections[direction,i];
                }
            }
            else
            {
                passedCheckup = false;
                break;
            }
        }
        if(passedCheckup)
        {   //establishing Common ancestor, there should be a better/faster way of doing this.
            int commonAncestor = parent[sourceindex];
            while (commonAncestor > 0 && (((binaryLocation[sourceindex, 0] ^ target[0]) & (1 << levelDepth[commonAncestor])) > 0
                ||(int)((binaryLocation[sourceindex, 1] ^ target[1]) & (1 << levelDepth[commonAncestor])) > 0
                ||((binaryLocation[sourceindex, 2] ^ target[2]) & (1 << levelDepth[commonAncestor])) > 0))
            {
                commonAncestor = parent[commonAncestor];
            }
            TraverseToNode(commonAncestor, target[0], target[1], target[2]);

     
            RenderNode(TraverseToNode(commonAncestor, target[0], target[1], target[2]), Color.cyan);
        }
 
    }

}


