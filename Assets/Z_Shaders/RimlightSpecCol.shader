Shader "Custom/RimlightSpecCol" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap ("Bumpmap", 2D) = "bump" {}
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
		}
		LOD 300
		
		CGPROGRAM
		     
			
			#pragma surface surf BlinnPhong 
			
			half _Shininess;
			
			struct Input {
				float dummy;
				float2 uv_MainTex;
				float2 uv_BumpMap;
				float3 viewDir;
			};
			float4 _RimColor;
			float _RimPower;
			sampler2D _MainTex;
			sampler2D _BumpMap;
			void surf (Input IN, inout SurfaceOutput o) {
				o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
				o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
				o.Gloss = 1;
				o.Specular = _Shininess;
				o.Alpha = 0;
				half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
				o.Emission = _RimColor.rgb * pow (rim, _RimPower);
			}
		ENDCG
	}
	FallBack "Transparent/VertexLit"
}