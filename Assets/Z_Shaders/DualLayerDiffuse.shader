﻿Shader "Custom/DualLayerDiffuse" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SecondTex ("Overlay (RGB)",2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
	
		

		struct Input {
			float2 uv_MainTex;
		};
		
		sampler2D _MainTex;
		
		void surf (Input IN, inout SurfaceOutput o) {
			
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
	
			
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
